# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

from pkg_resources import parse_version
from kaitaistruct import __version__ as ks_version, KaitaiStruct, KaitaiStream, BytesIO


if parse_version(ks_version) < parse_version('0.7'):
    raise Exception("Incompatible Kaitai Struct Python API: 0.7 or later is required, but you have %s" % (ks_version))

class Gamedata(KaitaiStruct):
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.count = self._io.read_u4le()
        self.paths = [None] * (self.count)
        for i in range(self.count):
            self.paths[i] = self._root.File(self._io, self, self._root)

        self.contents = [None] * (self.count)
        for i in range(self.count):
            self.contents[i] = self._io.read_bytes(self.paths[i].size)


    class File(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.size = self._io.read_u4le()
            self.len = self._io.read_u4le()
            self.offset = self._io.read_u4le()
            self.name = (self._io.read_bytes(self.len)).decode(u"ASCII")



