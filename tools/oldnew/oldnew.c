/// Tool to convert map format from PB to map format from pebble
///
/// Usage: oldnew <source> <destination>
/// source - path to the file to be converted
/// destination - where to save the file
///
/// Example: oldnew 9,8.poziom 9,8.pbmap
///
/// More information about the pebble map format can be found in documentation

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "map/location.h"
#include "map/obstacle_data.h"

struct surface *
assets_get_background(int id)
{
	(void)id;
	return NULL;
}

struct audio_music;
struct audio_music *
assets_get_music(void *ptr, int id)
{
	(void)ptr;
	(void)id;
	return NULL;
}

static int
location_load_old_format(struct location *location, const char *path)
{
	// Read from file
	FILE *f = fopen(path, "rb");
	if (!f)
		return 1;

	fseek(f, 0, SEEK_END);
	long size = ftell(f);
	fseek(f, 0, SEEK_SET);

	uint16_t *map = (uint16_t *)malloc(size);
	size /= sizeof(uint16_t);
	fread(map, sizeof(uint16_t), size, f);
	fclose(f);

	if (size < MAX_OBSTACLES) {
		fprintf(stderr, "size is smaller than MAX_OBSTACLES, incorrect format\n");
		return 1;
	}

	int8_t pos_x = 0;
	int8_t pos_y = -1;

	// Load obstacles
	for (int i = 0; i < MAX_OBSTACLES; i++) {
		if (i % LOCATION_WIDTH == 0) {
			pos_y++;
			pos_x = 0;
		} else {
			pos_x++;
		}

		uint16_t id = map[i];
		uint8_t flags = 0;
		if (id == 201) {
			id = 0;
		}

		bool solid, visible, casts_shadow;
		get_flags_for_obstacle_id(id, &solid, &visible, &casts_shadow);

		if (solid)
			flags |= FLAGS_IS_SOLID;
		if (!visible)
			flags |= FLAGS_IS_INVISIBLE;
		if (casts_shadow)
			flags |= FLAGS_CASTS_SHADOW;

		struct obstacle ob = (struct obstacle){.id = id, .flags = flags};

		location_set_obstacle(location, vec2i(pos_x, pos_y), ob);
	}

	// Create header
	struct location_header header;
	header.flags = 0;
	header.background = 0;
	header.music = 0;
	header.empty = 0;

	if (size >= 580) {
		header.background = (uint8_t)map[MAX_OBSTACLES];
		header.music = (uint8_t)map[MAX_OBSTACLES + sizeof(uint16_t)];
	} else {
		fprintf(stderr, "no background or music was found\n");
	}

	location->header = header;

	return 0;
}

int
main(int argc, char *argv[])
{
	if (argc < 3) {
		printf("Usage: oldnew <input> <output>\n");
		return 1;
	}

	char *input = argv[1];
	char *output = argv[2];

	struct location *loc = location_create();
	loc->pos.x = 0;
	loc->pos.y = 0;

	printf("%s -> %s\n", input, output);

	location_load_old_format(loc, input);
	location_save(loc, output);

	location_free(&loc);
	return 0;
}
