#include "label.h"

#include <assert.h>
#include <stddef.h>

#include "common/graphics/surface.h"
#include "common/graphics/text.h"
#include "common/math/vector.h"

struct surface *
_ui_label_color(struct ui_context *ctx, const char *text, struct color_rgb c)
{
	assert(ctx != NULL);

	if (!text)
		return NULL;

	struct text_render_settings text_settings;
	text_settings.height = 5;
	text_settings.outlined = false;
	text_settings.color = c;

	return text_render(ctx->font, text, &text_settings);
}

void
_ui_label_from(struct ui_context *ctx, struct surface *surf)
{
	assert(ctx != NULL);

	if (!surf)
		return;

	if (ctx->layout) {
		ui_push_cmd(ctx, ui_cmd_draw_surf(surf, ctx->layout->next, NULL, true));
		ui_layout_add_child(ctx, vec2i(surf->width, surf->height));
	}
}

void 
ui_label_outline(struct ui_context *ctx, const char *text, struct color_rgb t,
                 struct color_rgb o)
{
	assert(ctx != NULL);

	if (!text)
		return;

	struct text_render_settings text_settings;
	text_settings.height = 5;
	text_settings.outlined = true;
	text_settings.color = t;
	text_settings.outline_color = o;

	struct surface *surf = text_render(ctx->font, text, &text_settings);

	_ui_label_from(ctx, surf);
}

void
ui_label_color(struct ui_context *ctx, const char *text, struct color_rgb c)
{
	struct surface *surf;
	if ((surf = _ui_label_color(ctx, text, c)) == NULL) {
		return;
	}

	_ui_label_from(ctx, surf);
}

void
ui_label(struct ui_context *ctx, const char *text)
{
	struct color_rgb color = {255, 255, 255};
	ui_label_color(ctx, text, color);
}
