#include "menu.h"

#include <assert.h>
#include <stdio.h>

#include "common/graphics/color.h"
#include "common/util.h"

static const int PADDING = 4;

static void
ui_menu_add_child(struct ui_context *ctx, vec2i_t size)
{
	struct ui_menu *layout = (struct ui_menu *)ctx->layout;

	// Adjust menu width to child's width
	if (ctx->layout->rect.w < size.x)
		ctx->layout->rect.w = size.x + PADDING * 2;

	// Increase menu size by child size
	layout->rect.h += size.y;

	// Compute next child position
	layout->next.y += size.y + layout->child_padding;

	// Increase child count
	layout->count++;
}

static struct ui_layout *
ui_menu(vec2i_t pos, int child_padding)
{
	struct ui_menu *menu = ecalloc(1, sizeof(struct ui_menu));

	menu->rect = recti(pos.x, pos.y, PADDING * 2, 4);
	menu->next = vec2i(pos.x + PADDING, pos.y + PADDING);
	menu->add_child_impl = ui_menu_add_child;
	menu->child_padding = child_padding;

	return (struct ui_layout *)menu;
}

void
ui_menu_begin(struct ui_context *ctx, vec2i_t pos, int child_padding)
{
	assert(ctx != NULL);

	ctx->layout = ui_menu(pos, child_padding);

	// Reserve space for menu
	ctx->layout->queue_pos = ui_reserve_cmd(ctx);
}

void
ui_menu_end(struct ui_context *ctx)
{
	assert(ctx != NULL);

	struct ui_menu *layout = (struct ui_menu *)ctx->layout;

	// Remove last element's child_padding and add our own
	layout->rect.h += PADDING + (layout->count - 1) * layout->child_padding;

	ui_set_cmd(ctx, ui_cmd_draw_rect(layout->rect, color_rgba(0, 0, 0, 192)),
	           layout->queue_pos);

	free(ctx->layout);
	ctx->layout = NULL;
}
