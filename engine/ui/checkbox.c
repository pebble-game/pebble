#include "checkbox.h"

#include <assert.h>
#include <stdio.h>

#include "label.h"

static const int PADDING = 1;

bool
ui_checkbox(struct ui_context *ctx, const char *text, bool checked)
{
	assert(ctx != NULL);

	if (!text)
		return false;

	bool clicked = false;

	vec2i_t size = vec2i(0, 0);
	vec2i_t pos = ctx->layout->next;

	struct ui_style *style = ctx->style;

	// First we need to draw checkbox
	if (checked) {
		if (ctx->layout) {
			ui_push_cmd(ctx,
			            ui_cmd_draw_surf(style->style, pos, &style->checkbox_checked, false));
		}
	} else {
		if (ctx->layout) {
			ui_push_cmd(ctx, ui_cmd_draw_surf(style->style, pos, &style->checkbox, false));
		}
	}

	// Then we should add the size of the checked checkbox,
	// since its texture is usually the largest
	size.x += style->checkbox_checked.w + PADDING;

	// Padding
	pos.x += style->checkbox_checked.w + PADDING;

	// And it is time to render label
	struct surface *nofocus = _ui_label_color(ctx, text, (struct color_rgb){166, 166, 166});

	int text_width = nofocus->width;
	int text_height = nofocus->height;

	int pos_x = pos.x - style->checkbox_checked.w;
	int pos_y = pos.y;

	if (ctx->input->mouse.x >= pos_x && ctx->input->mouse.x < (pos.x + (int)nofocus->width) &&
	    ctx->input->mouse.y >= pos_y && ctx->input->mouse.y < (pos.y + (int)nofocus->height)) {
		surface_clean(&nofocus);

		struct surface *focus = _ui_label_color(ctx, text, (struct color_rgb){255, 255, 255});

		if (ctx->layout) {
			ui_push_cmd(ctx, ui_cmd_draw_surf(focus, pos, NULL, true));
		}

		if (input_mouse_button(ctx->input, MOUSE_BUTTON_LEFT, STATE_JUST_PRESSED)) {
			clicked = true;
		}
	} else {
		if (ctx->layout) {
			ui_push_cmd(ctx, ui_cmd_draw_surf(nofocus, pos, NULL, true));
		}
	}

	size.x += text_width;
	size.y += text_height;

	// We are done
	ui_layout_add_child(ctx, vec2i(size.x, size.y));

	return clicked;
}
