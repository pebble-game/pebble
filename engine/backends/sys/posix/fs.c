#include "../fs.h"

#include <assert.h>
#include <dirent.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "common/log.h"
#include "common/util.h"

struct fs_dir {
	DIR *d;
	struct dirent *dir;
	char *path;

	struct fs_file it;
};

int
fs_create_dir(const char *path)
{
	mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	return 0;
}

bool
fs_is_valid_path(const char *path)
{
	return access(path, F_OK) == 0;
}

bool
fs_is_directory(const char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISDIR(path_stat.st_mode);
}

bool
fs_is_file(const char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISREG(path_stat.st_mode);
}

struct fs_dir *
fs_walk_start(const char *path)
{
	assert(path != NULL);

	struct fs_dir *dir = ecalloc(1, sizeof(*dir));
	dir->d = opendir(path);

	dir->path = dstrcpy(path);

	return dir;
}

struct fs_file *
fs_walk_dir(struct fs_dir *dir)
{
	assert(dir != NULL);

	dir->dir = readdir(dir->d);

	if (!dir->dir) {
		return NULL;
	}

	free(dir->it.name);

	dir->it.name = dstrcpy(dir->dir->d_name);

	struct stat path_stat;
	char *path = dsprintf("%s/%s", dir->path, dir->it.name);
	stat(path, &path_stat);

	if (S_ISDIR(path_stat.st_mode))
		dir->it.type = FS_IS_DIRECTORY;
	else if (S_ISREG(path_stat.st_mode))
		dir->it.type = FS_IS_FILE;
	else
		dir->it.type = FS_IS_UNKNOWN;

	free(path);

	return &dir->it;
}

void
fs_walk_end(struct fs_dir *dir)
{
	assert(dir != NULL);

	closedir(dir->d);
	free(dir->path);
	free(dir);
}
