#include "../fs.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

#include "common/log.h"
#include "common/util.h"

static void
display_error()
{
	LPVOID msgbuf;
	DWORD error = GetLastError();

	FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
	                  FORMAT_MESSAGE_IGNORE_INSERTS,
	              NULL, error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR)&msgbuf, 0,
	              NULL);

	LOG_ERRORF("%s", (LPCSTR)msgbuf);
}

struct fs_dir {
	WIN32_FIND_DATA ffd;
	HANDLE handle;
	char *path;
	WINBOOL last;

	struct fs_file it;
};

int
fs_create_dir(const char *path)
{
	// NOTE(legalprisoner8140):
	// I'm not sure how it will work with characters outside of 7-bit ASCII,
	// so in case of an accident we will have to deal with it
	BOOL result = CreateDirectoryA(path, NULL);

	if (result == ERROR_ALREADY_EXISTS) {
		return FS_ALREADY_EXISTS;
	} else if (result == ERROR_PATH_NOT_FOUND) {
		return FS_PATH_INVALID;
	} else if (result != 0) {
		return FS_UNKNOWN;
	}

	return 0;
}

bool
fs_is_valid_path(const char *path)
{
	WIN32_FIND_DATA ffd;
	HANDLE handle = FindFirstFileA(path, &ffd);

	bool found = handle != INVALID_HANDLE_VALUE;

	if (found) {
		FindClose(handle);
	}

	return found;
}

bool
fs_is_directory(const char *path)
{
	DWORD attrs = GetFileAttributesA(path);
	return (attrs & FILE_ATTRIBUTE_DIRECTORY);
}

bool
fs_is_file(const char *path)
{
	DWORD attrs = GetFileAttributesA(path);
	return !(attrs & FILE_ATTRIBUTE_DIRECTORY);
}

struct fs_dir *
fs_walk_start(const char *path)
{
	assert(path != NULL);

	struct fs_dir *dir = ecalloc(1, sizeof(*dir));
	dir->last = 1;

	// FindFirstFile requires wildcard
	size_t size = strlen(path) + 3;
	dir->path = emalloc(size);
	snprintf(dir->path, size, "%s/*", path);

	dir->handle = FindFirstFileA(dir->path, &dir->ffd);

	if (dir->handle == INVALID_HANDLE_VALUE) {
		display_error();
		free(dir->path);
		free(dir);

		return NULL;
	}

	return dir;
}

struct fs_file *
fs_walk_dir(struct fs_dir *dir)
{
	assert(dir != NULL);

	// There are no more files
	if (dir->last == 0) {
		return NULL;
	}

	free(dir->it.name);

	dir->it.name = dstrcpy(dir->ffd.cFileName);

	if (dir->ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
		dir->it.type = FS_IS_DIRECTORY;
	} else {
		dir->it.type = FS_IS_FILE;
	}

	// Let's look for the next file, and save the result for later so we don't miss any file.
	dir->last = FindNextFileA(dir->handle, &dir->ffd);

	// Let's also handle errors, like a good boy
	DWORD error = GetLastError();
	if (error != ERROR_NO_MORE_FILES && error != ERROR_SUCCESS) {
		return NULL;
	}

	// If everything is okay, then let's return the file
	return &dir->it;
}

void
fs_walk_end(struct fs_dir *dir)
{
	assert(dir != NULL);

	FindClose(dir->handle);
	free(dir->path);
	free(dir);
}
