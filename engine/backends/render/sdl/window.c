#include "backends/render/window.h"

#include <SDL.h>
#include <SDL_joystick.h>

#include "common/util.h"

struct window {
	SDL_Surface *screen;
	SDL_Surface *streaming_texture;
	uint32_t streaming_texture_width;
	uint32_t streaming_texture_height;
	bool quit;

	// Joystick
	SDL_Joystick *joystick;

	// Size in pixels
	uint32_t width;
	uint32_t height;

	// Streaming texture display boundary
	struct recti display_boundary;

	int transform_stack_len;
};

struct window *
window_create(const char *title, int width, int height, int flags)
{
	assert(title != NULL);
	struct window *window = ecalloc(1, sizeof(*window));

	window->screen = SDL_SetVideoMode(width, height, 32, SDL_SWSURFACE);

	window->quit = 0;
	window->streaming_texture = NULL;

	if (SDL_NumJoysticks() >= 1) {
		window->joystick = SDL_JoystickOpen(0);
	} else {
		window->joystick = NULL;
	}

	if (window->joystick != NULL) {
		if (SDL_JoystickNumButtons(window->joystick) < 12) {
			SDL_JoystickClose(window->joystick);
			window->joystick = NULL;
		}
	}

	// to be set later in window_update function
	window->display_boundary.x = 0;
	window->display_boundary.y = 0;
	window->display_boundary.w = 1;
	window->display_boundary.h = 1;

	SDL_WM_SetCaption(title, NULL);

	return window;
}

void
window_free(struct window *window)
{
	assert(window != NULL);

	SDL_JoystickClose(window->joystick);
	SDL_FreeSurface(window->screen);

	free(window);
}

void
window_handle_events(struct window *window, SDL_Event *e)
{
	(void)e;
	(void)window;
}

void
window_render(struct window *window, struct surface *surface)
{
	assert(surface->pixel_format == SURFACE_PIXEL_FORMAT_RGB24);

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	Uint32 rmask = 0xff000000;
	Uint32 gmask = 0x00ff0000;
	Uint32 bmask = 0x0000ff00;
#else
	Uint32 rmask = 0x000000ff;
	Uint32 gmask = 0x0000ff00;
	Uint32 bmask = 0x00ff0000;
#endif

	if (window->streaming_texture) {
		// Surface size changed. Regenerate texture
		if (window->streaming_texture_width != surface->width ||
		    window->streaming_texture_height != surface->height) {
			SDL_FreeSurface(window->streaming_texture);
			window->streaming_texture = NULL;
		}
	}

	// Create texture if not exists
	if (!window->streaming_texture) {
		window->streaming_texture_width = surface->width;
		window->streaming_texture_height = surface->height;
		window->streaming_texture =
		    SDL_CreateRGBSurfaceFrom(surface->pixels, surface->width, surface->height, 24,
		                             surface->pitch, rmask, gmask, bmask, 0);
	}

	// Clear window
	SDL_FillRect(window->screen, NULL, 0x00000000);

	// NOTE(legalprisoner8140):
	// I'm not sure if this works
	int window_w, window_h;
	window_w = window->screen->w;
	window_h = window->screen->h;

	window->width = window_w;
	window->height = window_h;

	// Correct aspect ratio
	float surface_aspect_ratio = surface->width / (float)surface->height;
	float window_aspect_ratio = window_w / (float)window_h;

	SDL_Rect rect;

	if (surface_aspect_ratio < window_aspect_ratio) {
		rect.w = window_h * surface_aspect_ratio;
		rect.h = window_h;
		rect.y = 0;
		rect.x = window_w / 2 - rect.w / 2;
	} else {
		rect.w = window_w;
		rect.h = window_w / surface_aspect_ratio;
		rect.x = 0;
		rect.y = window_h / 2 - rect.h / 2;
	}

	window->display_boundary.x = rect.x;
	window->display_boundary.y = rect.y;
	window->display_boundary.w = rect.w;
	window->display_boundary.h = rect.h;

	// TODO(legalprisoner8140): scaling
	// However, the problem is that SDL 1.2 does not offer scaling, so we have a problem.
	// Anyone want to implement scaling?

	SDL_BlitSurface(window->streaming_texture, NULL, window->screen, NULL);
	SDL_Flip(window->screen);
}

struct recti
window_get_display_boundary(struct window *window)
{
	assert(window != NULL);

	return window->display_boundary;
}

vec2i_t
window_get_streaming_texture_res(struct window *window)
{
	assert(window != NULL);

	return vec2i(window->streaming_texture_width, window->streaming_texture_height);
}
