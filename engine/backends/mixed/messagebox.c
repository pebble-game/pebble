#include "messagebox.h"

#include "backends/render/render.h"

#ifdef _RENDER_BACKEND_sdl2
#include <SDL.h>
#elif defined(_WIN32)
#include <windows.h>
#endif

void
messagebox(const char *title, const char *message)
{
#ifdef _RENDER_BACKEND_sdl2
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, title, message, NULL);
#elif defined(_WIN32)
	MessageBoxA(NULL, message, title, MB_ICONERROR);
#else
	LOG_ERRORF("messagebox: (%s) %s", title, message);
#endif
}
