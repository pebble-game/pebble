#include "timing.h"

#include "backends/render/render.h"

// The priority is:
// - SDL2 or SDL
// - system native (in the future)
// - error

#ifdef _RENDER_BACKEND_sdl2
#define USE_SDL
#include <SDL.h>
#elif defined(_RENDER_BACKEND_sdl)
#define USE_SDL
#include <SDL.h>
#else
#error "No support for the current platform"
#endif

static uint64_t millis_start;

uint64_t
get_ticks()
{
#ifdef USE_SDL
	return SDL_GetTicks();
#endif
}

void
time_init()
{
	millis_start = get_ticks();
}

double
time_get_seconds()
{
	return time_get_millis() / 1000.0;
}

uint64_t
time_get_millis()
{
	return get_ticks() - millis_start;
}
