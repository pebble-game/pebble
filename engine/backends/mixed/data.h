/// @addtogroup game_save
/// @{

#ifndef DATA_H
#define DATA_H

#ifdef DATA_PRIVATE
char *_get_data_path();
#endif

/// @brief Returns path to game's directory. Prefer using game_save_get_path()
/// for files.
/// @return Valid path, needs to be freed later
/// @see game_save_get_path
char *get_data_path();

#endif

/// @}
