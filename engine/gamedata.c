#define MAKE_GD_FILE_PUBLIC
#include "gamedata.h"

#include <assert.h>

#include "common/log.h"
#include "common/serialization.h"
#include "common/util.h"

#define GAMEDATA_CHECK(expr)          \
	do {                              \
		if (expr)                     \
			return GD_FILE_CORRUPTED; \
	} while (0)

struct gd_static_priv {
	uint8_t *file_content;
};

struct gd_dynamic_priv {
	FILE *file;
	size_t file_size;
};

static int
gamedata_static(struct gamedata *gd, uint8_t *data, size_t size)
{
	assert(gd != NULL);
	assert(data != NULL);

	size_t p = 0;

	GAMEDATA_CHECK(sizeof(struct gd_header) > size);

	// Deserialize gamedata's header
	uint32_t count = adeserialize_u32(data, &p);
	uint32_t fc_offset = adeserialize_u32(data, &p);

	// LOG_INFOF("Count: %d", count);
	// LOG_INFOF("FC pos: %d", fc_offset);

	// Copy header
	gd->header.count = count;
	gd->header.fc_offset = fc_offset;

	// Offsets shouldn't be greater than gamedata
	GAMEDATA_CHECK(fc_offset > size);

	gd->files = NULL;

	struct gd_file *files = emalloc(count * sizeof(struct gd_file));

	// If it is less than zero, it means that the gamedata is probably
	// corrupted.
	int64_t bytes_to_eof = size - sizeof(struct gd_header);

	for (size_t i = 0; i < count; i++) {
		uint32_t file_size = adeserialize_u32(data, &p);
		uint8_t path_length = adeserialize_u8(data, &p) + 1;

		bytes_to_eof -= file_size + path_length + sizeof(uint32_t) + sizeof(uint8_t);
		GAMEDATA_CHECK(bytes_to_eof < 0);

		const char *path = (const char *)data + p;
		// Skip path
		p += path_length;

		uint32_t offset = adeserialize_u32(data, &p);
		GAMEDATA_CHECK(fc_offset + offset > size);

		/*
		LOG_INFOF("File %zu { file_size: %d, path_length: %d, path: \"%s\", offset: %d }",
		          i, file_size, path_length, path, offset);
		*/

		uint8_t *content = data + fc_offset + offset;

		files[i].path = path;
		files[i].content = (struct gd_content){.data = content, .size = file_size};
		files[i].offset = offset;

		shput(gd->files, path, &files[i]);
	}

	return GD_OK;
}

static int
gamedata_dynamic(struct gamedata *gd, FILE *f, size_t size)
{
	assert(gd != NULL);

	GAMEDATA_CHECK(sizeof(struct gd_header) > size);

	// Deserialize gamedata's header
	uint32_t count = fdeserialize_u32(f);
	uint32_t fc_offset = fdeserialize_u32(f);

	// LOG_INFOF("Count: %d", count);
	// LOG_INFOF("FC pos: %d", fc_offset);

	// Copy header
	gd->header.count = count;
	gd->header.fc_offset = fc_offset;

	// Offsets shouldn't be greater than gamedata
	GAMEDATA_CHECK(fc_offset > size);

	gd->files = NULL;

	struct gd_file *files = emalloc(count * sizeof(struct gd_file));

	// If it is less than zero, it means that the gamedata is probably
	// corrupted.
	int64_t bytes_to_eof = size - sizeof(struct gd_header);

	for (size_t i = 0; i < count; i++) {
		uint32_t file_size = fdeserialize_u32(f);
		uint8_t path_length = fdeserialize_u8(f) + 1;

		bytes_to_eof -= file_size + path_length + sizeof(uint32_t) + sizeof(uint8_t);
		GAMEDATA_CHECK(bytes_to_eof < 0);

		char *path = emalloc(path_length * sizeof(char));
		fread(path, sizeof(char), path_length, f);

		uint32_t offset = fdeserialize_u32(f);
		GAMEDATA_CHECK(fc_offset + offset > size);

		/*
		LOG_INFOF("File %zu { file_size: %d, path_length: %d, path: \"%s\", offset: %d }",
		          i, file_size, path_length, path, offset);
		*/

		files[i].path = path;
		files[i].content = (struct gd_content){.data = NULL, .size = file_size};
		files[i].offset = offset;

		shput(gd->files, path, &files[i]);
	}

	return GD_OK;
}

int
gamedata_from_file(struct gamedata *gd, enum gamedata_mode mode, const char *path)
{
	assert(gd != NULL);
	assert(path != NULL);

	gd->mode = mode;

	FILE *file;
	size_t file_size;

	file = fopen(path, "rb");
	if (!file)
		return GD_CANNOT_OPEN_FILE;

	fseek(file, 0, SEEK_END);
	file_size = ftell(file);
	fseek(file, 0, SEEK_SET);

	switch (mode) {
	case GD_MODE_STATIC: {
		uint8_t *file_content = emalloc(file_size * sizeof(uint8_t));
		fread(file_content, sizeof(uint8_t), file_size, file);

		fclose(file);

		struct gd_static_priv *priv = emalloc(sizeof(*priv));
		priv->file_content = file_content;
		gd->priv = priv;

		return gamedata_static(gd, file_content, file_size);
	}

	case GD_MODE_DYNAMIC: {
		struct gd_dynamic_priv *priv = emalloc(sizeof(*priv));
		priv->file = file;
		priv->file_size = file_size;
		gd->priv = priv;

		return gamedata_dynamic(gd, file, file_size);
	}
	}

	return GD_OK;
}

int
gamedata_from_memory(struct gamedata *gd, uint8_t *data, size_t size)
{
	gd->mode = GD_MODE_STATIC;
	return gamedata_static(gd, data, size);
}

struct gd_file *
gamedata_request(struct gamedata *gd, const char *path)
{
	assert(gd != NULL);
	assert(path != NULL);

	switch (gd->mode) {

	case GD_MODE_STATIC: {
		struct gd_file *f = shget(gd->files, path);
		return f;
	}

	case GD_MODE_DYNAMIC: {
		struct gd_file *f = shget(gd->files, path);
		if (!f)
			return NULL;

		// Is data already loaded?
		if (f->content.data != NULL)
			return f;

		// If not, then we need to load it.
		LOG_INFOF("Loading file %s", path);

		struct gd_dynamic_priv *priv = (struct gd_dynamic_priv *)gd->priv;
		FILE *file = priv->file;

		// Set file cursor position
		fseek(file, gd->header.fc_offset + f->offset, SEEK_SET);

		uint8_t *content = emalloc(f->content.size * sizeof(uint8_t));
		fread(content, sizeof(uint8_t), f->content.size, file);
		f->content.data = content;

		return f;
	}
	}

	return NULL;
}

size_t
gamedata_unload(struct gamedata *gd, const char *path)
{
	assert(gd != NULL);
	assert(path != NULL);

	switch (gd->mode) {

	case GD_MODE_STATIC:
		// Unloading files in static mode is impossible
		return 0;

	case GD_MODE_DYNAMIC: {
		struct gd_file *f = shget(gd->files, path);
		if (f == NULL || f->content.data == NULL)
			return 0;

		LOG_INFOF("Unloading file %s", path);
		free(f->content.data);
		return f->content.size;
	}
	}

	return 0;
}

void
gamedata_free(struct gamedata *gd)
{
	assert(gd != NULL);

	switch (gd->mode) {
	case GD_MODE_STATIC: {
		struct gd_static_priv *priv = (struct gd_static_priv *)gd->priv;
		free(priv->file_content);
		break;
	}

	case GD_MODE_DYNAMIC: {
		struct gd_dynamic_priv *priv = (struct gd_dynamic_priv *)gd->priv;
		fclose(priv->file);
		break;
	}
	}
}

struct gd_pointer
gamedata_open(struct gamedata *gd, const char *path)
{
	assert(gd != NULL);
	assert(path != NULL);
	assert(gd->mode == GD_MODE_DYNAMIC);

	// If file doesn't exists, return structure with everything
	// set to zero.
	struct gd_file *f = shget(gd->files, path);
	if (!f)
		return (struct gd_pointer){.offset = 0, .pos = 0, .size = 0};

	return (struct gd_pointer){.offset = f->offset, .pos = 0, .size = f->content.size};
}

int
gamedata_read(struct gamedata *gd, struct gd_pointer *ptr, uint8_t *where, size_t size)
{
	assert(gd != NULL);
	assert(ptr != NULL);
	assert(where != NULL);
	assert(size != 0);
	assert(gd->mode == GD_MODE_DYNAMIC);

	struct gd_dynamic_priv *priv = (struct gd_dynamic_priv *)gd->priv;
	fseek(priv->file, gd->header.fc_offset + ptr->offset + ptr->pos, SEEK_SET);

	// Check if user wants to load more than possible,
	// if so, load how much is left.
	if (ptr->pos + size > ptr->size) {
		int diff = ptr->size - ptr->pos;
		ptr->pos += diff;
		return fread(where, sizeof(uint8_t), diff, priv->file);
	}

	ptr->pos += size;
	return fread(where, sizeof(uint8_t), size, priv->file);
}

int
gamedata_seek(struct gd_pointer *ptr, long offset, int whence)
{
	assert(ptr != NULL);

	switch (whence) {
	case SEEK_SET: // Beginning of file
		// If the offset is greater than the size, return error
		if (offset > ptr->size) {
			return -1;
		}

		ptr->pos = offset;
		break;

	case SEEK_CUR: // Current position
		// If the offset plus current position is greater than the size, return error
		if (offset + ptr->pos > ptr->size) {
			return -1;
		}

		ptr->pos += offset;
		break;

	case SEEK_END: // End of file
		if (offset > ptr->size) {
			return -1;
		}

		ptr->pos = ptr->size - offset;
		break;
	}

	return 0;
}

long
gamedata_tell(struct gd_pointer *ptr)
{
	assert(ptr != NULL);

	return ptr->pos;
}

size_t
gamedata_save(struct gd_file *files, size_t count, const char *path)
{
	assert(files != NULL);
	assert(path != NULL);

	FILE *f = fopen(path, "wb");

	// Header
	fserialize_u32(f, count);

	uint32_t fc_offset = sizeof(uint32_t) * 2;
	for (size_t i = 0; i < count; i++) {
		// count, itself, file_size, offset and path_length
		fc_offset += sizeof(uint32_t) * 2 + sizeof(uint8_t);

		fc_offset += strlen(files[i].path) + 1;
	}

	fserialize_u32(f, fc_offset);

	// File Table
	uint32_t offset = 0;
	for (size_t i = 0; i < count; i++) {
		fserialize_u32(f, files[i].content.size);

		size_t length = strlen(files[i].path);
		fserialize_u8(f, length);
		fwrite(files[i].path, sizeof(char), length + 1, f);

		fserialize_u32(f, offset);
		offset += files[i].content.size;
	}

	// File Content
	for (size_t i = 0; i < count; i++) {
		fwrite(files[i].content.data, sizeof(uint8_t), files[i].content.size, f);
	}

	fclose(f);
	return 0;
}
