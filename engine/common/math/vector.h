/// @addtogroup math Math
/// @{

#ifndef VECTOR_H
#define VECTOR_H

struct vec2 {
	float x, y;
};
typedef struct vec2 vec2_t;

struct vec2i {
	int x, y;
};
typedef struct vec2i vec2i_t;

inline vec2_t
vec2(float x, float y)
{
	return (vec2_t){
	    .x = x,
	    .y = y,
	};
}

inline vec2i_t
vec2i(int x, int y)
{
	return (vec2i_t){
	    .x = x,
	    .y = y,
	};
}

inline vec2_t
to_vec2(vec2i_t v)
{
	return vec2(v.x, v.y);
}

inline vec2i_t
to_vec2i(vec2_t v)
{
	return vec2i(v.x, v.y);
}

#define VECTOR_OP_VV(funcname, op)                            \
	inline vec2_t funcname(vec2_t u, vec2_t v) \
	{                                                         \
		return vec2(u.x op v.x, u.y op v.y);                  \
	}

VECTOR_OP_VV(add2v, +)
VECTOR_OP_VV(sub2v, -)
VECTOR_OP_VV(mul2v, *)
VECTOR_OP_VV(div2v, /)

#undef VECTOR_OP_VV

#define VECTOR_OP_VF(funcname, op)                      \
	inline vec2_t funcname(vec2_t u, float x) \
	{                                                   \
		return vec2(u.x op x, u.y op x);                \
	}

VECTOR_OP_VF(add2f, +)
VECTOR_OP_VF(sub2f, -)
VECTOR_OP_VF(mul2f, *)
VECTOR_OP_VF(div2f, /)

#undef VECTOR_OP_VF

#endif

/// @}
