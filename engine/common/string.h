/// @addtogroup utility
/// @{

#ifndef STRING_H
#define STRING_H

/// @brief Converts null-terminated string to lowercase equivalent
/// @param str null-terminated string
/// @return Lowercase equivalent of str, NULL if str is also NULL
/// @note Returned string should be freed
char *strtolower(const char *str);

/// @brief Checks if string ends with given suffix.
/// @param str the null-terminated string to check
/// @param suffix the null-terminated string to suffix
/// @return 0 on success, -1 if str or suffix is NULL, or
/// if length of suffix is greater than length of string
int strendswith(const char *str, const char *suffix);

/// @brief Converts null-terminated string to a number.
/// @param str the null-terminated string to convert
/// @param dest the destination to which the result will be saved
/// @return 0 on success, -1 if string is not a number or is out of range
int strtoi(const char *str, int *dest);

/// @brief Dynamically allocates and creates a formatted string.
/// @param format The format is the same as in snprintf
/// @return Pointer to formatted string
/// @note Needs to be freed later
char *dsprintf(const char *format, ...);

char *dstrcpy(const char *str);

#endif

/// @}
