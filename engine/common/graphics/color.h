/// @addtogroup graphics
/// @{

#ifndef COLOR_H
#define COLOR_H

#include <stdint.h>

struct color_rgb {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

struct color_rgba {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

inline struct color_rgb
color_rgb(uint8_t r, uint8_t g, uint8_t b)
{
	return (struct color_rgb){
	    .r = r,
	    .g = g,
	    .b = b,
	};
}

inline struct color_rgba
color_rgba(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
{
	return (struct color_rgba){
	    .r = r,
	    .g = g,
	    .b = b,
	    .a = a,
	};
}

#endif

/// @}
