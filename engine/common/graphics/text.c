#include "text.h"

#include <ft2build.h>
#include <stdlib.h>
#include "stb/stb_ds.h"

#include "common/util.h"
#include "surface.h"
#include FT_FREETYPE_H

struct text_render_settings
text_render_settings_defaults()
{
	return (struct text_render_settings){
	    .height = 14,
	    .color = {255, 255, 255},
	    .outlined = false,
	    .outline_color = {0, 0, 0},
	};
}

struct font {
	FT_Library library;
	FT_Face face;
	void *font_binary_data;
};

struct font *
font_from_memory(void *data, size_t size)
{
	struct font *font = ecalloc(1, sizeof(struct font));

	int ret;

	ret = FT_Init_FreeType(&font->library);
	if (ret) {
		font_clean(&font);
		return NULL;
	}

	font->font_binary_data = emalloc(size);
	memcpy(font->font_binary_data, data, size);

	ret = FT_New_Memory_Face(font->library, font->font_binary_data, size, 0, &font->face);
	if (ret) {
		font_clean(&font);
		return NULL;
	}

	return font;
}

void
font_clean(struct font **_font)
{
	if (!_font)
		return;

	struct font *font = *_font;

	if (!font)
		return;

	if (font->face) {
		FT_Done_Face(font->face);
		font->face = NULL;
	}

	if (font->font_binary_data) {
		free(font->font_binary_data);
		font->font_binary_data = NULL;
	}

	if (font->library) {
		FT_Done_FreeType(font->library);
		font->library = NULL;
	}

	free(font);
	*_font = NULL;
}

static void
shadow_set(struct surface *surf, int x, int y, struct color_rgb color)
{
	x++;
	y++;
	((uint8_t *)surf->pixels)[y * surf->pitch + x * 4 + 0] = color.r;
	((uint8_t *)surf->pixels)[y * surf->pitch + x * 4 + 1] = color.g;
	((uint8_t *)surf->pixels)[y * surf->pitch + x * 4 + 2] = color.b;
	((uint8_t *)surf->pixels)[y * surf->pitch + x * 4 + 3] = 255;
}

struct surface *
text_render(struct font *font, const char *utf8, struct text_render_settings *settings)
{
	if (!font)
		return NULL;
	if (!utf8)
		return NULL;
	if (!settings)
		return NULL;

	int ret;

	ret = FT_Set_Pixel_Sizes(font->face, 0, settings->height);
	if (ret)
		return NULL;

	// For every character
	struct glyph {
		uint16_t ch;
		uint32_t posX;
		uint32_t posY;
		uint8_t *glyph_data;
		uint32_t glyph_width;
		uint32_t glyph_pitch;
		uint32_t glyph_height;
		int left;
		int top;
		struct surface *surf_glyph;
	};

	size_t len = strlen(utf8);
	if (len == 0) {
		font_clean(&font);
		return NULL;
	}

	struct glyph *glyphs = NULL;
	// TODO: Convert to proper UTF-8.
	for (size_t i = 0; i < len; i++) {
		struct glyph glyph = {0};
		glyph.ch = utf8[i];
		arrput(glyphs, glyph);
	}

	uint32_t posX = 0;
	size_t glyphs_size = arrlen(glyphs);
	for (size_t i = 0; i < glyphs_size; i++) {
		struct glyph *g = glyphs + i;
		ret = FT_Load_Char(font->face, g->ch, FT_LOAD_RENDER | FT_LOAD_TARGET_MONO);
		if (ret)
			continue; // Invalid or unknown character code

		g->glyph_width = font->face->glyph->bitmap.width;
		g->glyph_pitch = font->face->glyph->bitmap.pitch;
		g->glyph_height = font->face->glyph->bitmap.rows;
		g->posX = posX;
		g->left = font->face->glyph->bitmap_left;
		g->top = font->face->glyph->bitmap_top;

		size_t glyph_data_size = g->glyph_pitch * g->glyph_height;
		g->glyph_data = emalloc(glyph_data_size);
		memcpy(g->glyph_data, font->face->glyph->bitmap.buffer, glyph_data_size);
		posX += g->glyph_width + 1;
	}

	uint32_t surf_width = 0;
	uint32_t surf_height = 0;
	int surf_top = settings->height / 4;
	for (size_t i = 0; i < glyphs_size; i++) {
		struct glyph *g = glyphs + i;

		g->posY = -g->top + settings->height;

		int right = g->posX + g->glyph_width;
		if (right > (int)surf_width)
			surf_width = right;

		int bottom = g->posY + g->glyph_height;
		if (bottom > (int)surf_height)
			surf_height = bottom;

		int top = g->posY;
		if (top < surf_top)
			surf_top = top;
	}

	struct surface *surface =
	    surface_create_empty(surf_width, surf_height + surf_top, SURFACE_PIXEL_FORMAT_RGBA32);

	// Fill surface with text
	uint8_t *pixels = surface->pixels;
	for (size_t i = 0; i < glyphs_size; i++) {
		struct glyph *g = glyphs + i;
		for (uint32_t y = 0; y < g->glyph_height; y++) {
			for (uint32_t x = 0; x < g->glyph_width; x++) {
				int xx = g->posX + x;
				int yy = g->posY + y - surf_top;
				if (xx < 0 || yy < 0 || xx >= (int)surface->width || yy >= (int)surface->height)
					continue;

				uint8_t packed = g->glyph_data[y * g->glyph_pitch + x / 8];

				uint8_t brightness = ((packed >> ((7 - x) % 8)) & 1) * 255;
				pixels[yy * surface->pitch + xx * 4 + 0] =
				    (brightness * settings->color.r) / 255;
				pixels[yy * surface->pitch + xx * 4 + 1] =
				    (brightness * settings->color.g) / 255;
				pixels[yy * surface->pitch + xx * 4 + 2] =
				    (brightness * settings->color.b) / 255;
				pixels[yy * surface->pitch + xx * 4 + 3] = brightness;
			}
		}
	}

	if (settings->outlined) {
		struct surface *shadowed_surface = surface_create_empty(
		    surface->width + 2, surface->height + 2, SURFACE_PIXEL_FORMAT_RGBA32);

		// Create outline
		uint8_t *pixels = surface->pixels;
		for (uint32_t y = 0; y < surface->height; y++) {
			for (uint32_t x = 0; x < surface->width; x++) {
				// Pick alpha channels
				uint8_t center = pixels[y * surface->pitch + x * 4 + 3];
				if (center == 255) {
					shadow_set(shadowed_surface, x - 1, y, settings->outline_color);
					shadow_set(shadowed_surface, x + 1, y, settings->outline_color);
					shadow_set(shadowed_surface, x, y - 1, settings->outline_color);
					shadow_set(shadowed_surface, x, y + 1, settings->outline_color);
				}

				uint8_t top = 0, bottom = 0, left = 0, right = 0;

				if (y > 0)
					top = pixels[(y - 1) * surface->pitch + x * 4 + 3];
				if (y < surface->height - 1)
					bottom = pixels[(y + 1) * surface->pitch + x * 4 + 3];
				if (x > 0)
					left = pixels[y * surface->pitch + (x - 1) * 4 + 3];
				if (x < surface->width - 1)
					right = pixels[y * surface->pitch + (x + 1) * 4 + 3];

				if (top == 255 || bottom == 255 || left == 255 || right == 255) {
					pixels[y * surface->pitch + x * 4 + 3] = 254;
				}
			}
		}

		surface_blit(surface, NULL, shadowed_surface, vec2i(1, 1));
		surface_clean(&surface);
		surface = shadowed_surface;
	}

	// Free glyph data
	for (size_t i = 0; i < glyphs_size; i++) {
		struct glyph *g = glyphs + i;
		free(g->glyph_data);
		g->glyph_data = NULL;
	}
	arrfree(glyphs);
	return surface;
}
