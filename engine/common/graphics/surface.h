/// @addtogroup graphics Graphics
/// @{

#ifndef SURFACE_H
#define SURFACE_H

#include <stddef.h>
#include <stdint.h>

#include "../math/rect.h"
#include "../math/vector.h"

#define FULL_OPT      __attribute__((no_sanitize("undefined"))) __attribute__((hot))
#define ALWAYS_INLINE __attribute__((always_inline)) inline

enum surface_pixel_format { SURFACE_PIXEL_FORMAT_RGB24, SURFACE_PIXEL_FORMAT_RGBA32 };

enum surface_blending_mode {
	SURFACE_BLENDING_MODE_NONE,
	SURFACE_BLENDING_MODE_BLEND,
	SURFACE_BLENDING_MODE_ADD,
	SURFACE_BLENDING_MULTIPLY
};

struct surface {
	void *pixels;                             // read/write
	uint32_t pitch;                           // read only
	uint32_t width;                           // read only
	uint32_t height;                          // read only
	enum surface_pixel_format pixel_format;   // read only
	enum surface_blending_mode blending_mode; // read only
	bool keyed;                               // read only
	// uint8_t key_r;                            // read only
	// uint8_t key_g;                            // read only
	// uint8_t key_b;                            // read only
	uint32_t key;
	uint8_t alpha; // read only
};

/// @brief Create empty surface
/// @param width Width. Set to 0 to make pixels unallocated.
/// @param height Height. Set to 0 to make pixels unallocated.
/// @param format Surface pixel format (24-bit RGB or 32-bit RGBA)
/// @returns newly created surface
struct surface *surface_create_empty(uint32_t width, uint32_t height,
                                     enum surface_pixel_format format);

/// @brief Create surface from memory (e.g. PNG data)
struct surface *surface_from_memory(void *data, size_t size);

/// @brief Color keying for RGB surfaces
void surface_set_colorkey(struct surface *surface, bool enable, uint8_t red, uint8_t green,
                          uint8_t blue);

void surface_clean(struct surface **surface);

/// @brief Blit surface source into dest
/// @param source Source surface
/// @param srcrect Rectangle of source surface. Can be NULL.
/// @param dest Destination surface
/// @param dstpos Position of render.
void surface_blit(struct surface *source, const struct recti *srcrect, struct surface *dest,
                  vec2i_t dstpos);

/// @brief Helper function for rendering surface flipped in X axis.
/// Source surface is rendered fully.
/// This function is relatively expensive. Use with sprites or static data only.
void surface_blit_flip_x(struct surface *source, struct surface *dest, vec2i_t dstpos);

/// @brief Helper function for rendering surface flipped in X axis.
/// Source surface is rendered fully.
/// This function is relatively expensive. Use with sprites or static data only.
void surface_blit_flip_y(struct surface *source, struct surface *dest, vec2i_t dstpos);

/// @brief Fill surface rectangle with color
/// @param rect Can be NULL
void surface_fill(struct surface *dest, struct recti *rect, uint8_t r, uint8_t g, uint8_t b,
                  uint8_t a);

void surface_fill_blend(struct surface *dest, struct recti *rect, uint8_t r, uint8_t g,
                        uint8_t b, uint8_t a);

/// @brief Get pixel directly. NOT SAFE!
void surface_get_pixel(struct surface *surface, uint32_t x, uint32_t y, uint8_t *r, uint8_t *g,
                       uint8_t *b, uint8_t *a);

FULL_OPT ALWAYS_INLINE void
surface_get_pixel24(struct surface *surface, uint32_t x, uint32_t y, uint8_t *r, uint8_t *g,
                    uint8_t *b)
{
	const int pitch = surface->pitch;
	*r = ((uint8_t *)surface->pixels)[y * pitch + x * 3 + 0];
	*g = ((uint8_t *)surface->pixels)[y * pitch + x * 3 + 1];
	*b = ((uint8_t *)surface->pixels)[y * pitch + x * 3 + 2];
}

/// @brief Get pixel directly, with boundary checking.
void surface_get_pixel_safe(struct surface *surface, int x, int y, uint8_t *r, uint8_t *g,
                            uint8_t *b, uint8_t *a);

/// @brief Set pixel directly. NOT SAFE!
void surface_set_pixel(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b,
                       uint8_t a);

FULL_OPT ALWAYS_INLINE void
surface_set_pixel24(struct surface *surface, int x, int y, uint8_t r, uint8_t g, uint8_t b)
{
	const int pitch = surface->pitch;
	((uint8_t *)surface->pixels)[y * pitch + x * 3 + 0] = r;
	((uint8_t *)surface->pixels)[y * pitch + x * 3 + 1] = g;
	((uint8_t *)surface->pixels)[y * pitch + x * 3 + 2] = b;
}

/// @brief Set pixel directly, with boundary checking.
void surface_set_pixel_safe(struct surface *surface, int x, int y, uint8_t r, uint8_t g,
                            uint8_t b, uint8_t a);

/// @brief Blend pixel with boundary checking
void surface_blend_pixel_safe(struct surface *surface, int x, int y, uint8_t r, uint8_t g,
                              uint8_t b, uint8_t a);

/// @brief Set surface blending operation
void surface_set_blending_mode(struct surface *surface, enum surface_blending_mode mode);

/// @brief Set surface alpha. SURFACE_BLENDING_MODE_BLEND required.
void surface_set_alpha(struct surface *surface, uint8_t alpha);

#endif

/// @brief Create a rotated (and scaled) surface from input surface.
/// @param angle Angle in radians
/// @param scale_x X-Scale, 1.0 is the default
/// @param scale_y Y-Scale, 1.0 is the default
/// @warning Make sure to free the output surface after usage.
struct surface *surface_rotoscope(struct surface *surface, float angle, float scale_x,
                                  float scale_y);

/// @}
