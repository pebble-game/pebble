/// @addtogroup logging Logging
/// @{

#ifndef LOG_H
#define LOG_H

#define LOG_INFO(msg)                                                   \
	do {                                                                \
		fprintf(stderr, "[LOG] (%s:%i) %s\n", __FILE__, __LINE__, msg); \
	} while (0)
#define LOG_ERROR(msg)                                                  \
	do {                                                                \
		fprintf(stderr, "[ERR] (%s:%i) %s\n", __FILE__, __LINE__, msg); \
	} while (0)
#define LOG_INFOF(msg, ...)                                                          \
	do {                                                                             \
		fprintf(stderr, "[LOG] (%s:%i) " msg "\n", __FILE__, __LINE__, __VA_ARGS__); \
	} while (0)
#define LOG_ERRORF(msg, ...)                                                         \
	do {                                                                             \
		fprintf(stderr, "[ERR] (%s:%i) " msg "\n", __FILE__, __LINE__, __VA_ARGS__); \
	} while (0)

#endif

/// @}
