#include "minunit.h"
int tests_run = 0;

#include <stdint.h>
#include <stdio.h>

#include "common/log.h"
#include "map/location.h"

struct surface *
assets_get_background(int id)
{
	return NULL;
}

static const uint8_t loc_good[] = {0x00, 0x13, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00,
                                   0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00};

static char *
test_load()
{
	struct location *loc = location_create();
	loc->pos.x = 0;
	loc->pos.y = 0;

	location_load_array(loc, (const uint8_t *)loc_good, sizeof(loc_good));

	mu_assert("ERROR, invalid 'flags' in header, should be 0", loc->header.flags == 0);
	mu_assert("ERROR, invalid 'background' in header, should be 0x13",
	          loc->header.background == 0x13);
	mu_assert("ERROR, invalid 'music' in header, should be 1", loc->header.music == 1);
	mu_assert("ERROR, invalid 'empty' in header, should be 0", loc->header.empty == 0);
	return 0;
}

static char *
all_tests()
{
	mu_run_test(test_load);
	return 0;
}

int
main(int argc, char **argv)
{
	char *result = all_tests();

	if (result != 0)
		fprintf(stderr, "%s\n", result);
	else
		fprintf(stderr, "LOCATION: ALL TESTS PASSED\n");

	fprintf(stderr, "Tests run: %d\n", tests_run);
	return result != 0;
}
