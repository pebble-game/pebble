#include "push_anim.h"

#include "assets.h"
#include "common/graphics/surface.h"
#include "common/math/util.h"
#include "map/obstacle_data.h"

static void
render(struct particle_push_anim *part, float step)
{
	float anim = lerpf(part->anim_prev, part->anim, step);

	vec2_t pos;
	pos.x = lerpf(part->target_pos.x, part->position.x, anim);
	pos.y = lerpf(part->target_pos.y, part->position.y, anim);

	vec2i_t visual_pos = world_get_visual_pos(part->world, pos);

	struct surface *surf = assets_get_obstacle(ID_PUSH_HV);
	if (!surf)
		return;

	surface_blit(surf, NULL, part->world->game->screen, visual_pos);
}

static void
update(struct particle_push_anim *part)
{
	part->anim_prev = part->anim;
	part->anim *= 0.7f;
	if (part->anim < 0.05f) {
		part->remove = true;
	}
}

struct particle_push_anim *
particle_push_anim_create(struct world *world, vec2_t position,
                          vec2_t target_position)
{
	struct particle_push_anim *part = ecalloc(1, sizeof(struct particle_push_anim));

	particle_init(part, world, position, render, update, NULL);

	part->target_pos = target_position;
	part->anim_prev = 1.0f;
	part->anim = 1.0f;

	return part;
}
