#include "smoke.h"

#include "common/math/vector.h"
#include "common/util.h"

static void
render(struct particle_smoke *part, float step)
{
	(void)step;

	vec2i_t pos = world_get_visual_pos(part->world, part->position);

	uint8_t alpha = (part->remaining_lifetime / (float)part->set_lifetime) * 255.0f;

	// particle shape
	//  X_X
	//  _X_
	//  X_X
	const vec2i_t offsets1[] = {vec2i(0, 0), vec2i(-1, -1), vec2i(1, -1), vec2i(-1, 1),
	                                 vec2i(1, 1)};

	// particle shape
	//  _X_
	//  X_X
	//  _X_
	const vec2i_t offsets2[] = {vec2i(0, -1), vec2i(-1, 0), vec2i(1, 0), vec2i(0, 1)};

	uint32_t count;
	const vec2i_t *offsets;

	// Interpolate every tick (60 times/s)
	if (part->remaining_lifetime % 2) {
		offsets = offsets1;
		count = sizeof(offsets1) / sizeof(offsets1[0]);
	} else {
		offsets = offsets2;
		count = sizeof(offsets2) / sizeof(offsets2[0]);
	}

	for (uint32_t i = 0; i < count; i++) {
		surface_blend_pixel_safe(part->world->game->screen, pos.x + offsets[i].x,
		                         pos.y + offsets[i].y, part->red, part->green, part->blue,
		                         alpha);
	}
}

static void
update(struct particle_smoke *part)
{
	part->remaining_lifetime--;
	if (part->remaining_lifetime == 0) {
		part->remove = true;
		return;
	}

	part->position.x += part->velocity.x;
	part->position.y += part->velocity.y;

	part->velocity.x *= 0.95f;
	part->velocity.y *= 0.95f;
}

struct particle_smoke *
particle_smoke_create(struct world *world, vec2_t position, vec2_t velocity,
                      uint8_t red, uint8_t green, uint8_t blue, uint16_t lifetime)
{
	struct particle_smoke *part = ecalloc(1, sizeof(struct particle_smoke));

	particle_init(part, world, position, render, update, NULL);

	part->velocity = velocity;
	part->set_lifetime = lifetime;
	part->remaining_lifetime = lifetime;
	part->red = red;
	part->green = green;
	part->blue = blue;
	return part;
}
