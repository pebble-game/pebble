#ifndef PART_COLORED_H
#define PART_COLORED_H

#include "particle.h"

struct particle_colored {
	PARTICLE_HEADER
	vec2_t velocity;
	uint16_t lifetime;
	uint8_t red;
	uint8_t green;
	uint8_t blue;
	bool bouncing;
	uint32_t bounce_count;
};

struct particle_colored *particle_colored_create(struct world *world, vec2_t position,
                                                 vec2_t velocity, uint8_t red,
                                                 uint8_t green, uint8_t blue);

#endif
