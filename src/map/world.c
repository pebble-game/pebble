#include "world.h"

#include <float.h>
#include <math.h>
#include <stdlib.h>
#include "stb/stb_ds.h"

#include "backends/audio/audio.h"
#include "common/log.h"
#include "common/math/util.h"
#include "entities/entity.h"
#include "entities/player.h"
#include "location_processor.h"
#include "map/location.h"
#include "particles/particle.h"

struct world *
world_create(struct game *game)
{
	struct world *world = ecalloc(1, sizeof(struct world));

	world->game = game;
	world->space = physics_space_create(world);

	return world;
}

void
world_clean(struct world **_world)
{
	if (!_world)
		return;

	struct world *world = *_world;

	// Free all entities
	for (int i = 0; i < arrlen(world->entities); i++) {
		entity_clean(world->entities[i]);
	}
	arrfree(world->entities);
	world->entities = NULL;

	// Free all particles
	for (int i = 0; i < arrlen(world->particles); i++) {
		particle_clean(world->particles[i]);
	}
	arrfree(world->particles);
	world->particles = NULL;

	physics_space_clean(&world->space);

	audio_music_clean(&world->playing_music);
	world->playing_music_id = 0;

	// Free all processed locations
	while (arrlen(world->loaded_locations) > 0) {
		world_unload_location(world, arrlast(world->loaded_locations)->pos);
	}

	arrfree(world->loaded_locations);
	world->loaded_locations = NULL;

	arrfree(world->players);

	free(world);

	*_world = NULL;
}

static void
update_entities(struct world *world)
{
	// Update entities
	int entity_count = arrlen(world->entities);
	for (int i = 0; i < entity_count; i++) {
		struct entity *entity = world->entities[i];
		if (entity->remove)
			continue;
		entity_update(entity);
	}

	// Remove entities marked for removal
	for (int i = arrlen(world->entities) - 1; i >= 0; i--) {
		struct entity *entity = world->entities[i];
		if (!entity->remove)
			continue;

		// Entity remove flag is active, remove entity
		entity_clean(entity);
		arrdel(world->entities, i);
		// LOG_INFOF("Entity removed, size %td", arrlen(world->entities));
	}
}

static void
update_particles(struct world *world)
{
	int particle_count = arrlen(world->particles);
	for (int i = 0; i < particle_count; i++) {
		struct particle *particle = world->particles[i];
		if (particle->remove)
			continue;
		particle_update(particle);
	}

	// Remove particles marked for removal
	for (int i = 0; i < arrlen(world->particles); i++) {
		struct particle *particle = world->particles[i];
		if (!particle->remove)
			continue;

		// Particle remove flag is active, remove particle
		particle_clean(particle);
		arrdelswap(world->particles, i);
		// printf("Particle removed, size %d\n", (int)arrlen(world->particles));
	}
}

static void
push_stack(struct world *world)
{
	transform_stack_push(&world->game->tstack, (struct transform){
	                                               .translate = world->camera_pos,
	                                           });
}

static void
pop_stack(struct world *world)
{
	transform_stack_pop(&world->game->tstack);
}

static void
location_get_shift(struct world *world, struct location *loc, int *shiftX, int *shiftY)
{
	vec2_t pos;
	pos.x = loc->pos.x * LOCATION_WIDTH * OBSTACLE_SCALE - 0.1f;
	pos.y = loc->pos.y * LOCATION_HEIGHT * OBSTACLE_SCALE - 0.1f;
	vec2i_t visual_pos = world_get_visual_pos(world, pos);
	*shiftX = visual_pos.x;
	*shiftY = visual_pos.y;
}

void
world_update(struct world *world)
{
	// Update camera motion
	vec2i_t camera = {
	    .x = world->current_loc_pos.x * LOCATION_WIDTH * OBSTACLE_SCALE,
	    .y = world->current_loc_pos.y * LOCATION_HEIGHT * OBSTACLE_SCALE,
	};

	uint32_t distX = abs(world->camera_pos.x - camera.x);
	uint32_t distY = abs(world->camera_pos.y - camera.y);

	if (distX > 320 || distY > 320) {
		// Too long distance, reset scrolling
		world->camera_pos.x = camera.x;
		world->camera_pos.y = camera.y;
	}

	uint32_t speed = 1 + (distX + distY) / 10;
	if (speed > 8)
		speed = 8;

	if (world->camera_pos.x < camera.x)
		world->camera_pos.x += speed;

	if (world->camera_pos.x > camera.x)
		world->camera_pos.x -= speed;

	if (world->camera_pos.y < camera.y)
		world->camera_pos.y += speed;

	if (world->camera_pos.y > camera.y)
		world->camera_pos.y -= speed;

	// Used for picking color at pixel coordinates in some particles. Do not remove.
	push_stack(world);

	update_entities(world);
	update_particles(world);

	physics_space_tick(world->space);

	// Location cleanup (remove offscreen locations)
	int location_count = arrlen(world->loaded_locations);
	for (int i = location_count - 1; i >= 0; i--) {
		struct location *loc = world->loaded_locations[i];

		if (loc->pos.x == world->current_loc_pos.x && loc->pos.y == world->current_loc_pos.y) {
			loc->offscreen_time_ticks = 0;
		} else {
			loc->offscreen_time_ticks++;
			if (loc->offscreen_time_ticks > 60) {
				world_unload_location(world, loc->pos);
			}
		}
	}

	// Background change animation
	if (world->loc_pos_change_bg_fade > 0.0f) {
		world->loc_pos_change_bg_fade *= 0.88f;

		if (world->loc_pos_change_bg_fade < 0.005f)
			world->loc_pos_change_bg_fade = 0.0f;
	}

	// Do not remove.
	pop_stack(world);
}

void
world_render(struct world *world, struct surface *target, float step)
{
	int shiftX, shiftY;

	push_stack(world);

	uint32_t loaded_locations_count = arrlen(world->loaded_locations);

	for (uint32_t i = 0; i < loaded_locations_count; i++) {
		// Render background for currently loaded location
		struct location *loc = world->loaded_locations[i];

		bool is_prev =
		    loc->pos.x == world->previous_loc_pos.x && loc->pos.y == world->previous_loc_pos.y;
		bool is_cur =
		    loc->pos.x == world->current_loc_pos.x && loc->pos.y == world->current_loc_pos.y;

		if (world->loc_pos_change_bg_fade > 0.0f) {
			if (is_prev)
				location_render_background(loc, target, 0, 0, 255);
			if (is_cur)
				location_render_background(loc, target, 0, 0,
				                           255 - world->loc_pos_change_bg_fade * 255);
		} else {
			if (is_cur)
				location_render_background(loc, target, 0, 0, 255);
		}

		if (loc->pos.x == world->previous_loc_pos.x &&
		    loc->pos.y == world->previous_loc_pos.y) {}
	}

	// Render every loaded location
	for (uint32_t i = 0; i < loaded_locations_count; i++) {
		struct location *loc = world->loaded_locations[i];
		location_get_shift(world, loc, &shiftX, &shiftY);
		location_render(loc, target, shiftX, shiftY);
	}

	// render entities in reverse order because we want the player to be on top
	// and the player is always the first object added to the world
	int entity_count = arrlen(world->entities);
	for (int i = entity_count - 1; i >= 0; i--) {
		entity_render(world->entities[i], target, step);
	}

	int particle_count = arrlen(world->particles);
	for (int i = 0; i < particle_count; i++) {
		particle_render(world->particles[i], step);
	}

	// Render postprocess for every loaded location
	for (uint32_t i = 0; i < loaded_locations_count; i++) {
		struct location *loc = world->loaded_locations[i];
		location_get_shift(world, loc, &shiftX, &shiftY);
		location_render_postprocess(loc, target, shiftX, shiftY);
	}

	pop_stack(world);
}

void
world_spawn_entity(struct world *world, void *entity)
{
	struct entity *ent = entity;
	assert(ent != NULL);
	arrput(world->entities, ent);
	// LOG_INFOF("Entity added, size %td", arrlen(world->entities));
}

void
world_spawn_particle(struct world *world, void *particle)
{
	struct particle *part = particle;
	assert(part != NULL);
	arrput(world->particles, part);
	// printf("Particle added, size %d\n", (int)arrlen(world->particles));
}

void
world_go_to(struct world *world, vec2i_t loc_pos)
{
	// Location not changed
	if (loc_pos.x == world->current_loc_pos.x && loc_pos.y == world->current_loc_pos.y)
		return;

	int len = arrlen(world->entities);
	for (int i = 0; i < len; ++i) {
		struct entity *e = world->entities[i];
		if (e->remove_offscreen)
			e->remove = true;
	}

	world->loc_pos_change_bg_fade = 1.0f;
	world->previous_loc_pos = world->current_loc_pos;
	world->current_loc_pos = loc_pos;

	struct location *loc = world_load_location(world, world->current_loc_pos);
	if (!loc) {
		// The player goes to a non-existent location, so we create a new location based on the
		// previous one to preserve the background and effects for post-processing.
		struct location *prev_loc = world_get_location(world, world->previous_loc_pos);

		loc = location_create();
		loc->background_id = prev_loc->background_id;
		loc->header = prev_loc->header;
		loc->pos = vec2i(loc_pos.x, loc_pos.y);

		arrput(world->loaded_locations, loc);
		return;
	}

	if (loc->header.music != world->playing_music_id) {
		world->playing_music_id = loc->header.music;
		assets_free_music_ptr(world->playing_music);
		world->playing_music = assets_get_music(world->game, world->playing_music_id);
		audio_music_play(world->playing_music, true);
	}
}

struct obstacle *
world_get_obstacle(struct world *world, vec2_t global_pos)
{
	vec2i_t location_pos, obstacle_pos;

	vec2i_t global_obstacle_pos;
	global_obstacle_pos.x = global_pos.x / OBSTACLE_SCALE;
	global_obstacle_pos.y = global_pos.y / OBSTACLE_SCALE;

	location_pos.x = global_obstacle_pos.x / LOCATION_WIDTH;
	location_pos.y = global_obstacle_pos.y / LOCATION_HEIGHT;
	obstacle_pos.x = global_obstacle_pos.x % LOCATION_WIDTH;
	obstacle_pos.y = global_obstacle_pos.y % LOCATION_HEIGHT;

	struct location *loc = world_get_location(world, location_pos);
	if (!loc)
		return NULL;

	return location_get_obstacle(loc, obstacle_pos);
}

struct location *
world_get_location(struct world *world, vec2i_t location_pos)
{
	int loaded_location_count = arrlen(world->loaded_locations);
	for (int i = 0; i < loaded_location_count; i++) {
		struct location *loc = world->loaded_locations[i];
		if (loc->pos.x == location_pos.x && loc->pos.y == location_pos.y) {
			return loc;
		}
	}
	return NULL;
}

struct location *
world_load_location(struct world *world, vec2i_t location_pos)
{
	bool deleted = world_unload_location(world, location_pos);

	LOG_INFOF("Loading location (%i, %i)", location_pos.x, location_pos.y);

	struct location *new_processed_loc = location_load_process(world, location_pos, deleted);
	if (new_processed_loc == NULL) {
		LOG_INFO("Location doesn't exist");
		return NULL;
	}

	arrput(world->loaded_locations, new_processed_loc);
	LOG_INFOF("Loaded locations count: %td", arrlen(world->loaded_locations));
	return new_processed_loc;
}

bool
world_unload_location(struct world *world, vec2i_t location_pos)
{
	bool deleted = false;

	for (int i = 0; i < arrlen(world->loaded_locations); i++) {
		struct location *loc = world->loaded_locations[i];
		if (loc->pos.x != location_pos.x || loc->pos.y != location_pos.y)
			continue;

		location_free(&loc);
		arrdel(world->loaded_locations, i);
		deleted = true;
		break;
	}

	return deleted;
}

vec2i_t
world_get_visual_pos(struct world *world, vec2_t global_pos)
{
	vec2i_t stack_pos = transform_stack_get_translation(&world->game->tstack);
	return vec2i(global_pos.x - stack_pos.x + 0.01f, global_pos.y - stack_pos.y + 0.01f);
}

vec2_t
world_get_global_pos(vec2i_t location_pos, vec2i_t obstacle_pos)
{
	return vec2((location_pos.x * LOCATION_WIDTH + obstacle_pos.x) * OBSTACLE_SCALE,
	            (location_pos.y * LOCATION_HEIGHT + obstacle_pos.y) * OBSTACLE_SCALE);
}

vec2i_t
world_get_global_posi(vec2i_t location_pos, vec2i_t obstacle_pos)
{
	return vec2i((location_pos.x * LOCATION_WIDTH + obstacle_pos.x) * OBSTACLE_SCALE,
	             (location_pos.y * LOCATION_HEIGHT + obstacle_pos.y) * OBSTACLE_SCALE);
}

vec2i_t
world_get_visual_pos2(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	vec2i_t stack_pos = transform_stack_get_translation(&world->game->tstack);
	return vec2i((location_pos.x * LOCATION_WIDTH + obstacle_pos.x) * OBSTACLE_SCALE -
	                 stack_pos.x + 0.1f,
	             (location_pos.y * LOCATION_HEIGHT + obstacle_pos.y) * OBSTACLE_SCALE -
	                 stack_pos.y + 0.1f);
}

vec2i_t
world_get_pos_in_location(vec2_t global_pos)
{
	vec2i_t pos = to_vec2i(global_pos);
	pos.x = pos.x % (LOCATION_WIDTH * OBSTACLE_SCALE);
	pos.y = pos.y % (LOCATION_HEIGHT * OBSTACLE_SCALE);
	return pos;
}

vec2i_t
world_get_obstacle_pos_round(vec2_t global_pos)
{
	int x = roundf(global_pos.x / OBSTACLE_SCALE);
	int y = roundf(global_pos.y / OBSTACLE_SCALE);

	vec2i_t pos = to_vec2i(global_pos);
	pos.x = x % LOCATION_WIDTH;
	pos.y = y % LOCATION_HEIGHT;

	return pos;
}

struct player *
world_get_closest_player(struct world* world, vec2_t global_pos) {
	struct player *closest_player = NULL;
	float closest_player_distance = FLT_MAX;

	// Find closest player
	const uint32_t player_count = arrlen(world->players);
	for (uint32_t i = 0; i < player_count; i++) {
		struct player *p = world->players[i];
		// Player center position
		float pX = p->body->position.x + p->body->size.x / 2.0f;
		float pY = p->body->position.y + p->body->size.y / 2.0f;

		// Calculate distance between flying X and player
		float distance = sqrtf(powf(global_pos.x - pX, 2.0f) + powf(global_pos.y - pY, 2.0f));
		if (distance < closest_player_distance) {
			closest_player_distance = distance;
			closest_player = p;
		}
	}

	return closest_player;
}
