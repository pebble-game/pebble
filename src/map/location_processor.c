#include "location_processor.h"

#include "stb/stb_ds.h"

#include "assets.h"
#include "entities/absorber.h"
#include "entities/bat.h"
#include "entities/blue_brick.h"
#include "entities/bomber.h"
#include "entities/bubble.h"
#include "entities/canon.h"
#include "entities/falling_block.h"
#include "entities/flying_x.h"
#include "entities/global_key.h"
#include "entities/grass.h"
#include "entities/green_ball.h"
#include "entities/kolota.h"
#include "entities/neon.h"
#include "entities/orange.h"
#include "entities/paper_brick.h"
#include "entities/push.h"
#include "entities/savepoint.h"
#include "entities/skeleton.h"
#include "entities/slime.h"
#include "entities/snakeblock.h"
#include "entities/spinning_ball.h"
#include "entities/teleporter.h"
#include "entities/timed_block.h"
#include "entities/trampoline.h"
#include "entities/turtle.h"
#include "entities/vanish.h"
#include "entities/water.h"
#include "map/location.h"
#include "map/world.h"
#include "obstacle_data.h"

struct processing_state {
	struct teleporter *teleporter;
};

static inline void
process_obstacle(struct world *world, struct location *loc, struct processing_state *state,
                 struct obstacle *o, vec2i_t obstacle_pos, bool refresh)
{
	vec2i_t location_pos = loc->pos;

	// NOTE(legalprisoner8140):
	// I recommend using this instead of location_position and obstacle_position.
	vec2_t global_pos = world_get_global_pos(location_pos, obstacle_pos);

	switch (o->id) {

	case ID_SAVEPOINT: {
		struct savepoint *sp = savepoint_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)sp);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_SPINNING_BALL: {
		struct obstacle ob;
		ob.flags = FLAGS_IS_SOLID | FLAGS_CASTS_SHADOW;
		ob.id = ID_WHITE_BLOCK;
		location_set_obstacle(loc, obstacle_pos, ob);

		struct spinning_ball *ball = spinning_ball_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, ball);
		break;
	}

	case ID_TELEPORTER: {
		struct teleporter *t = teleporter_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)t);

		if (state->teleporter == NULL) {
			state->teleporter = t;
		} else {
			state->teleporter->dest = t;
			t->dest = state->teleporter;
		}

		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	// NOTE(lqdev):
	// oh god please give me back my Nim ranges ;-;
	case ID_KEY1:
	case ID_KEY2:
	case ID_KEY3:
	case ID_KEY4:
	case ID_KEY5:
	case ID_KEY6:
	case ID_KEY7:
	case ID_KEY8:
	case ID_KEY9:
	case ID_KEY10: {
		int key_number = o->id - ID_KEY1;
		struct global_key *k = global_key_create(world, global_pos, key_number);
		world_spawn_entity(world, (struct entity *)k);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_KEYBLOCK1:
	case ID_KEYBLOCK2:
	case ID_KEYBLOCK3:
	case ID_KEYBLOCK4:
	case ID_KEYBLOCK5:
	case ID_KEYBLOCK6:
	case ID_KEYBLOCK7:
	case ID_KEYBLOCK8:
	case ID_KEYBLOCK9:
	case ID_KEYBLOCK10: {
		int required_keys = o->id - ID_KEYBLOCK1 + 1;
		if (world->keys_collected >= required_keys) {
			o->id = ID_KEYBLOCK_OPEN;
			o->flags &= ~FLAGS_IS_SOLID;
		}
		break;
	}

	case ID_FALLING_BLOCK: {
		struct falling_block *e = falling_block_create(world, loc->pos, obstacle_pos);
		world_spawn_entity(world, e);
		break;
	}

	case ID_VANISH: {
		struct vanish *v = vanish_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)v);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_PUSH_HV:
	case ID_PUSH_V:
	case ID_PUSH_H: {
		bool horiz = false, vert = false;
		switch (o->id) {
		case ID_PUSH_HV:
			horiz = true;
			vert = true;
			break;
		case ID_PUSH_H:
			horiz = true;
			break;
		case ID_PUSH_V:
			vert = true;
			break;
		}

		struct push *p = push_create(world, location_pos, obstacle_pos, horiz, vert);
		world_spawn_entity(world, (struct entity *)p);
		break;
	}

	case ID_BLUE_BRICK: {
		struct blue_brick *b = blue_brick_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)b);
		break;
	}

	case ID_BUBBLE: {
		struct bubble *b = bubble_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)b);
		location_remove_obstacle(loc, obstacle_pos);
		o->flags = FLAGS_IS_SOLID;
		break;
	}

	case ID_TALLGRASS: {
		struct grass *g = grass_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)g);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_TALLGRASS_DIRT: {
		struct grass *g = grass_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)g);
		struct obstacle ob = obstacle_null();
		ob.id = ID_DIRT_DIM;
		location_set_obstacle(loc, obstacle_pos, ob);
		break;
	}

	case ID_FLYING_X: {
		struct flying_x *x = flying_x_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)x);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_WATER: {
		if (!refresh) {
			// Spawn water entity only if not refreshed
			struct water *w = water_create(world, location_pos, obstacle_pos);
			world_spawn_entity(world, (struct entity *)w);
		}
		break;
	}

	case ID_ABSORBER: {
		struct absorber *a = absorber_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)a);
		break;
	}

	case ID_PAPER_BRICK: {
		struct paper_brick *p = paper_brick_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)p);
		break;
	}

	case ID_SNAKEBLOCK_GENERATOR: {
		struct sb_generator *sbg = sb_generator_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)sbg);
		break;
	}

	case ID_SNAKEBLOCK_BLOCK: {
		struct snakeblock *sb = snakeblock_create(world, location_pos, obstacle_pos);
		world_spawn_entity(world, (struct entity *)sb);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_TURTLE: {
		struct turtle *t = turtle_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)t);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_TIMED_BLOCK1: 
	case ID_TIMED_BLOCK2: 
	case ID_TIMED_BLOCK3: 
	case ID_TIMED_BLOCK4: 
	case ID_TIMED_BLOCK5: {
		int number = o->id - ID_TIMED_BLOCK1;
		struct timed_block *t = timed_block_create(world, global_pos, number);
		world_spawn_entity(world, (struct entity *)t);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_ORANGE_CRYSTAL: {
		struct orange *o = orange_create(world, global_pos);
		world_spawn_entity(world, (struct orange *)o);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_BOMBER: {
		struct bomber *b = bomber_create(world, global_pos);
		world_spawn_entity(world, (struct bomber *)b);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	/*
	case ID_BAT: {
		struct bat *b = bat_create(world, global_pos);
		world_spawn_entity(world, (struct bat *)b);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}
	*/

	case ID_KOLOTA: {
		struct kolota *k = kolota_create(world, global_pos, location_pos);
		world_spawn_entity(world, (struct entity *)k);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_GREEN_BALL: {
		struct green_ball *g = green_ball_create(world, global_pos);
		world_spawn_entity(world, (struct green_ball *)g);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_SLIME: {
		struct slime *s = slime_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)s);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_TRAMPOLINE: {
		struct trampoline *t = trampoline_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)t);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_CANON_DOWN:
	case ID_CANON_LEFT:
	case ID_CANON_RIGHT:
	case ID_CANON_UP: {
		int direction;

		const vec2i_t obstacle_below = vec2i(obstacle_pos.x, obstacle_pos.y+1);
		struct obstacle *ob = location_get_obstacle(loc, obstacle_below);
		const uint16_t ob_id = ob ? ob->id : ID_NONE;

		switch(ob_id) {
		case ID_SPIKE_RIGHT:
			direction = CANON_TARGET_POSITION;
			location_remove_obstacle(loc, obstacle_below);
			break;
		case ID_SPIKE_UP:
			direction = CANON_TARGET_PLAYER;
			location_remove_obstacle(loc, obstacle_below);
			break;
		default:
			direction = o->id - ID_CANON_LEFT;
		}

		struct canon *c = canon_create(world, global_pos, direction);
		world_spawn_entity(world, (struct entity *)c);
		o->id = ID_NONE;
		break;
	}

	case ID_SKELETON: {
		struct skeleton *s = skeleton_create(world, global_pos);
		world_spawn_entity(world, (struct entity *)s);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	case ID_NEON: {
		struct neon *n = neon_create(world, global_pos);
		// Fill empty cell with water
		if (!refresh) {
			struct water *w = water_create(world, location_pos, obstacle_pos);
			world_spawn_entity(world, (struct water  *)w);
		}
		world_spawn_entity(world, (struct neon *)n);
		location_remove_obstacle(loc, obstacle_pos);
		break;
	}

	default:
		break;
	}
}

struct location *
location_load_process(struct world *world, vec2i_t location_pos, bool refresh)
{
	struct location *readonly_loc = NULL;

	// Find readonly location
	for (int i = 0; i < arrlen(g_assets.readonly_locations); i++) {
		struct location *l = g_assets.readonly_locations[i];
		if (l->pos.x != location_pos.x || l->pos.y != location_pos.y)
			continue;
		readonly_loc = l;
		break;
	}

	if (!readonly_loc)
		return NULL;

	struct location *new_loc = location_create();
	location_copy_data(readonly_loc, new_loc);

	struct processing_state state = {0};
	for (int y = 0; y < LOCATION_HEIGHT; ++y) {
		for (int x = 0; x < LOCATION_WIDTH; ++x) {
			process_obstacle(world, new_loc, &state,
			                 &new_loc->obstacles[x + y * LOCATION_WIDTH], vec2i(x, y), refresh);
		}
	}

	return new_loc;
}
