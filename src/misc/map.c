#include "map.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "stb/stb_ds.h"

#include "common/log.h"
#include "common/util.h"

/// @return 0 on success, -1 if id is invalid number
static int
parse(char *entry, struct map_entry *dest)
{
	assert(entry != NULL);
	assert(dest != NULL);

	char *elem;

	// Key
	elem = strtok(entry, ":");

	// Convert key to int
	int key = 0;
	if (strtoi(elem, &key) < 0)
		return -1;

	// Value
	elem = strtok(NULL, ":");

	// Copy string
	char *value = dstrcpy(elem);

	// Remove newline
	value[strcspn(value, "\n")] = 0;

	dest->key = key;
	dest->value = value;

	return 0;
}

struct map_entry *
map_create(const char *path)
{
	if (!path)
		return NULL;

	struct map_entry *map = NULL;
	int line = 1;

	// 256 for value and 256 for key
	const size_t buffer_size = 512;

	char buffer[buffer_size];
	FILE *f = fopen(path, "r");

	while (fgets(buffer, buffer_size, f)) {
		struct map_entry entry;

		if (parse(buffer, &entry) < 0)
			goto err;

		hmput(map, entry.key, entry.value);
		line++;
	}

	fclose(f);
	return map;
err:
	LOG_ERRORF("Invalid mapping in %s:%d", path, line);
	hmfree(map);
	fclose(f);
	return NULL;
}

void
map_free(struct map_entry *map)
{
	assert(map != NULL);

	for (size_t i = 0; i < hmlenu(map); i++) {
		free(map[i].value);
	}

	hmfree(map);
}
