#ifndef MAP_H
#define MAP_H

// TODO(legalprisoner8140):
// Add support for gamedata

struct map_entry {
	int key;
	char *value;
};

/// @brief Loads a hashmap from a file.
/// @param path Path to a hashmap. If path is NULL, then function will return NULL.
/// @return Valid stbds hashmap, NULL if path is invalid or if id map is invalid.
/// @see map_free
struct map_entry *map_create(const char *path);

/// @brief Frees a hashmap.
void map_free(struct map_entry *map);

#endif
