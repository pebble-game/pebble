#ifndef BOMBER_H
#define BOMBER_H

#include "./entity.h"
#include "map/world.h"

struct bomber {
	ENTITY_HEADER

	struct physics_body *body;
	bool right;
	int tick;

	struct surface *bullet;
};

struct bomber *bomber_create(struct world *world, vec2_t global_pos);

#endif
