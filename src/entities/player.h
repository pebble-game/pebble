#ifndef ENTITIES_PLAYER_H
#define ENTITIES_PLAYER_H

#include <stdint.h>

#include "assets.h"
#include "entity.h"
#include "map/world.h"
#include "physics/physics.h"

enum facing {
	FACING_LEFT = 0,
	FACING_RIGHT = 1,
};

#define CHEAT_ENABLED(var, cheat) (var & cheat)
enum {
	// bitfield
	CHEAT_NOHACK = 0x0,
	CHEAT_FLYHACK = 0x1,
	CHEAT_GODMODE = 0x2
};

struct player {
	ENTITY_HEADER

	struct physics_body *body;

	// movement
	bool is_in_kinetic;
	bool is_in_water;
	bool is_in_water_prev; // Previous value of is_in_water, used for triggering e.g. splash
	                       // sounds
	vec2_t respawn_point;

	// Additional damped velocity
	vec2_t virtual_velocity;

	// animation
	enum player_animation animation;
	int animation_frame, animation_timer;
	enum facing facing;
	unsigned shooting_animation_timer;

	// shooting
	struct projectile *bullets[9];
	int max_bullets; // 0..8

	uint8_t cheats;
};

struct player *player_create(struct world *world, vec2_t position);

void player_set_pos(struct player *p, vec2i_t pos);
void player_get_location_pos(struct player *p, vec2i_t *out_pos);

void player_kill(struct player *p);

#endif
