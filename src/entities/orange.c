#include "orange.h"

#include <math.h>

#include "player.h"
#include "projectile.h"
#include "physics/collision_groups.h"

static void
clean(struct orange *o)
{
	o->body->remove = true;
}

static void
render(struct orange *o, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_ORANGE_CRYSTAL);

	entity_util_draw_sprite(o->world->game, s, o->body, sprite, vec2i(0, 0), step);
}

static void
update(struct orange *o)
{
	o->tick++;

	o->wave = sinf(o->tick * 0.1f) * 20.0f;
	o->body->position.y = o->y + o->wave;

	o->body->position.x -= 2.0f;
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct orange *o = (struct orange *)bp->user;
	struct projectile *proj = (struct projectile *)bc->user;

	projectile_destroy(proj);
	o->hp--;
}

static void
collide_with_player(struct physics_body *bp, struct physics_body *bc)
{
	(void)bp;

	struct player *p = (struct player *)bc->user;

	player_kill(p);
}

struct orange *
orange_create(struct world *world, vec2_t global_pos)
{
	struct orange *o = ecalloc(1, sizeof(*o));

	entity_init(o, world, render, update, clean);

	o->body = physics_body_create(world->space);
	o->body->position = global_pos;
	o->body->size = vec2(10, 10);
	o->body->user = o;

	o->wave = 0.0f;
	o->hp = 2;
	o->y = o->body->position.y;

	physics_body_body_callback(o->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);
	physics_body_body_callback(o->body, collide_with_player, BODY_COLLISION_PLAYER);

	return o;
}
