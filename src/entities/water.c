#include "water.h"

#include <math.h>
#include "stb/stb_ds.h"

#include "entities/player.h"
#include "map/location.h"
#include "map/world.h"
#include "physics/collision_groups.h"
#include "physics/physics.h"

static void
render(struct water *water, struct surface *dest, float step)
{
	(void)step;

	vec2i_t visual_pos =
	    world_get_visual_pos2(water->world, water->location_pos, water->obstacle_pos);

	if (visual_pos.x < -OBSTACLE_SCALE || visual_pos.y < -OBSTACLE_SCALE ||
	    visual_pos.x >= LOCATION_WIDTH * OBSTACLE_SCALE ||
	    visual_pos.y >= LOCATION_HEIGHT * OBSTACLE_SCALE) {
		// Water outside screen bounds, do not render
		return;
	}

	uint8_t red, green, blue;
	uint32_t ired, igreen, iblue;
	uint16_t posX, posY;

	for (uint8_t y = 0; y < OBSTACLE_SCALE; y++) {
		for (uint8_t x = 0; x < OBSTACLE_SCALE; x++) {
			posX = visual_pos.x + x;
			posY = visual_pos.y + y;

			// Boundary checking
			if (posX >= dest->width || posY >= dest->height)
				continue;

			surface_get_pixel24(dest, posX, posY, &red, &green, &blue);

			// Pixel color effects
			ired = (red * red) * 2 / 255;
			igreen = (green * green) * 2 / 255;
			iblue = (blue * blue) * 3 / 255;

			if (ired > 255)
				ired = 255;
			if (igreen > 255)
				igreen = 255;
			if (iblue > 255)
				iblue = 255;

			surface_set_pixel24(dest, posX, posY, ired, igreen, iblue);
		}
	}
}

static void
update(struct water *water)
{
	// Check if water is outside
	bool outside = true;
	uint32_t loaded_locations_count = arrlen(water->world->loaded_locations);
	for (uint32_t i = 0; i < loaded_locations_count; i++) {
		struct location *loc = water->world->loaded_locations[i];
		if (loc->pos.x == water->location_pos.x && loc->pos.y == water->location_pos.y) {
			outside = false;
			break;
		}
	}

	if (outside) {
		water->remove = true;
	}

	water->ticks++;
}

struct water *
water_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct water *water = ecalloc(1, sizeof(*water));

	entity_init(water, world, render, update, NULL);
	water->location_pos = location_pos;
	water->obstacle_pos = obstacle_pos;
	water->remove_offscreen = false;

	return water;
}
