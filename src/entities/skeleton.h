#ifndef SKELETON_H
#define SKELETON_H

#include "./entity.h"
#include "map/world.h"

struct skeleton {
	ENTITY_HEADER

	struct physics_body *body_top;
	struct physics_body *body_bottom;
	bool goright;
};

struct skeleton *skeleton_create(struct world *world, vec2_t global_pos);

#endif
