#include "flying_x.h"

#include <math.h>
#include "stb/stb_ds.h"

#include "assets.h"
#include "entities/player.h"
#include "map/world.h"
#include "physics/collision_groups.h"
#include "projectile.h"

static void
render(struct flying_x *x, struct surface *dest, float step)
{
	(void)step;
	surface_blit(assets_get_obstacle(ID_FLYING_X), NULL, dest,
	             world_get_visual_pos(x->world, x->position));
}

static void
update(struct flying_x *x)
{
	// Follow closest player
	const vec2_t body_center = {
		x->body->position.x + x->body->size.x / 2.0f,
		x->body->position.y + x->body->size.y / 2.0f,
	};
	struct player *closest_player = world_get_closest_player(x->world, body_center);

	if (!closest_player)
		return;

	float nX = closest_player->body->position.x + closest_player->body->size.x / 2.0f -
	           x->body->size.x / 2.0f;
	float nY = closest_player->body->position.y + closest_player->body->size.y / 2.0f -
	           x->body->size.y / 2.0f;

	if (nX < x->position.x)
		x->position.x -= 0.2f;

	if (nX > x->position.x)
		x->position.x += 0.2f;

	if (nY < x->position.y)
		x->position.y -= 0.2f;

	if (nY > x->position.y)
		x->position.y += 0.2f;

	x->body->position = x->position;
}

static void
clean(struct flying_x *x)
{
	x->body->remove = true;
}

static void
collide_with_player(struct physics_body *body_x, struct physics_body *body_player)
{
	(void)body_x;
	struct player *player = body_player->user;
	player_kill(player);
}

static void
collide_with_projectile(struct physics_body *body_x, struct physics_body *body_projectile)
{
	struct flying_x *x = body_x->user;
	struct projectile *proj = body_projectile->user;

	projectile_destroy(proj);

	x->health--;

	if (x->health == 0) {
		x->remove = true;
	}
}

struct flying_x *
flying_x_create(struct world *world, vec2_t global_pos)
{
	struct flying_x *x = ecalloc(1, sizeof(*x));

	entity_init(x, world, render, update, clean);

	x->position = global_pos;
	x->body = physics_body_create(world->space);
	x->body->size = vec2(10.0f, 10.0f);
	x->body->position = x->position;
	x->body->user = x;
	x->health = 2;

	physics_body_body_callback(x->body, collide_with_player, BODY_COLLISION_PLAYER);
	physics_body_body_callback(x->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return x;
}
