#ifndef TRAMPOLINE_H
#define TRAMPOLINE_H

#include "./entity.h"
#include "map/world.h"

struct trampoline {
	ENTITY_HEADER

	struct physics_body *body_top;
	struct physics_body *body_bottom;
	int offsetY;
};

struct trampoline *trampoline_create(struct world *world, vec2_t global_pos);

#endif
