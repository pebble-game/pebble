#ifndef SLIME_H
#define SLIME_H

#include "./entity.h"
#include "map/world.h"

struct slime {
	ENTITY_HEADER

	struct physics_body *body;
	bool right;
	int hp;
};

struct slime *slime_create(struct world *world, vec2_t global_pos);

#endif
