#ifndef KOLOTA_H
#define KOLOTA_H

#include "./entity.h"
#include "map/world.h"

struct kolota {
	ENTITY_HEADER

	struct physics_body *body;
	struct physics_body *lava;
	vec2_t pos;
	vec2i_t location_pos;
	int hp;
	int offsetX;
	int tick;
};

struct kolota *kolota_create(struct world *world, vec2_t global_pos, 
                             vec2i_t location_pos);

#endif
