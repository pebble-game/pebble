#include "bomber.h"

#include "physics/collision_groups.h"
#include "projectile.h"

static void
clean(struct bomber *b)
{
	b->body->remove = true;
	surface_clean(&b->bullet);
}

static void
render(struct bomber *b, struct surface *s, float step)
{
	struct surface *sprite = assets_get_obstacle(ID_BOMBER);

	entity_util_draw_sprite(b->world->game, s, b->body, sprite, vec2i(0, 0), step);
}

static void
update(struct bomber *b)
{
	b->tick++;

	b->body->velocity.x = b->right ? 2.0f : -2.0f;

	if (b->tick % 40 == 0) {
		vec2_t pos = b->body->position;
		pos.x += 2.0f;
		pos.y += 20.0f;

		// TODO: finish it

		/*
		struct projectile *pr =
		    projectile_create(b->world, pos, vec2(0.0f, 2.25f), vec2(10.0f, 20.0f),
		                      PROJECTILE_TARGET_PLAYER, b->bullet);

		world_spawn_entity(b->world, (struct entity *)pr);
		*/
	}

	if (physics_body_collision(b->body, WALL_RIGHT, JUST_STARTED_COLLIDING)) {
		b->right = true;
	} else if (physics_body_collision(b->body, WALL_LEFT, JUST_STARTED_COLLIDING)) {
		b->right = false;
	}
}

struct bomber *
bomber_create(struct world *world, vec2_t global_pos)
{
	struct bomber *b = ecalloc(1, sizeof(*b));

	entity_init(b, world, render, update, clean);

	b->body = physics_body_create(world->space);
	b->body->position = global_pos;
	b->body->size = vec2(20, 20);
	b->body->user = b;
	b->right = true;

	// As long as we are not able to rotate, we have to enjoy the black rectangle.
	b->bullet = surface_create_empty(10, 20, SURFACE_PIXEL_FORMAT_RGB24);
	struct recti rect = recti(0, 0, 10, 20);
	surface_fill(b->bullet, &rect, 0, 0, 0, 255);

	return b;
}
