#ifndef TIMED_BLOCK_H
#define TIMED_BLOCK_H

#include "./entity.h"
#include "map/world.h"

struct timed_block {
	ENTITY_HEADER

	int number;
	int tick;
	vec2_t pos;
	bool show;
	int last_number;
};

struct timed_block *timed_block_create(struct world *world, vec2_t global_pos, int number);

#endif
