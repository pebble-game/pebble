#include "blue_brick.h"

#include "common/math/vector.h"
#include "map/location.h"
#include "map/world.h"
#include "particles/colored.h"
#include "physics/collision_groups.h"
#include "projectile.h"
#include "sound_system.h"

#define BLUE_BRICK_HP 2

/*
static void
render(struct blue_brick *b, struct surface *s, float step)
{
}

static void
update(struct blue_brick *b)
{
}
*/

static void
clean(struct blue_brick *b)
{
	struct location *loc = world_get_location(b->world, b->location_pos);
	if (loc)
		location_remove_obstacle(loc, b->obstacle_pos);

	b->body->remove = true;
}

static void
collide_with_projectile(struct physics_body *bp, struct physics_body *bc)
{
	struct blue_brick *b = (struct blue_brick *)bp->user;
	struct projectile *proj = (struct projectile *)bc->user;

	projectile_destroy(proj);
	b->hp--;

	if (b->hp == 0) {
		b->remove = true;

		// Generate break particles
		for (int i = 0; i < 100; i++) {
			uint8_t brightness = rand() % 256;
			vec2_t pos = b->body->position;
			pos.x += rand() % 10;
			pos.y += rand() % 10;
			vec2_t vel;
			vel.x = (rand() % 1000 - 500) * 0.001f;
			vel.y = (rand() % 1000 - 500) * 0.001f - 0.1f;

			world_spawn_particle(b->world, particle_colored_create(b->world, pos, vel, 0,
			                                                       brightness, brightness));
		}

		// Play break sound
		sound_system_play(b->world->game->sound_sys, SAMPLE_BREAK);
	} else {
		// Play hit sound
		sound_system_play(b->world->game->sound_sys, SAMPLE_HIT);
	}
}

struct blue_brick *
blue_brick_create(struct world *world, vec2i_t location_pos, vec2i_t obstacle_pos)
{
	struct blue_brick *b = ecalloc(1, sizeof(*b));

	entity_init(b, world, NULL, NULL, clean);

	b->body = physics_body_create(world->space);
	b->body->position = world_get_global_pos(location_pos, obstacle_pos);

	b->body->size = vec2(10, 10);
	b->body->user = b;

	b->location_pos = location_pos;
	b->obstacle_pos = obstacle_pos;
	b->hp = BLUE_BRICK_HP;

	// Only projectiles interest us
	physics_body_body_callback(b->body, collide_with_projectile, BODY_COLLISION_PROJECTILES);

	return b;
}
