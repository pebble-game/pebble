#ifndef FLYING_X_H
#define FLYING_X_H

#include "./entity.h"
#include "map/world.h"
#include "physics/physics.h"

struct flying_x {
	ENTITY_HEADER

	vec2_t position;

	struct physics_body *body;

	uint32_t health;
};

struct flying_x *flying_x_create(struct world *world, vec2_t global_pos);

#endif
