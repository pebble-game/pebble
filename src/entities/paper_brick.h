#ifndef PAPER_BRICK_H
#define PAPER_BRICK_H

#include "./entity.h"
#include "map/world.h"

struct paper_brick {
	ENTITY_HEADER

	struct physics_body *body;
	vec2i_t location_pos;
	vec2i_t obstacle_pos;
};

struct paper_brick *paper_brick_create(struct world *world, vec2i_t location_pos,
                                       vec2i_t obstacle_pos);

#endif
