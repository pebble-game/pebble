// NOTE:
// No, this is not a typo.

#ifndef CANON_H
#define CANON_H

#include "./entity.h"
#include "map/world.h"

enum canon_type {
	CANON_LEFT = 0,
	CANON_UP = 1,
	CANON_RIGHT = 2,
	CANON_DOWN = 3,
	CANON_TARGET_POSITION = 4,
	CANON_TARGET_PLAYER = 5,
};

struct canon {
	ENTITY_HEADER

	struct physics_body *body;
	int type;
	int tick;
	vec2_t bullet_vel;
	vec2_t body_center;
	float offset;
	bool position_target_found;
};

struct canon *canon_create(struct world *world, vec2_t global_pos, int type);

#endif
