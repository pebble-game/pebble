#ifndef EDITOR_STATE_H
#define EDITOR_STATE_H

#include <stdbool.h>

#include "assets.h"
#include "backends/render/render.h"
#include "common/common.h"
#include "game.h"
#include "map/location.h"
#include "states/states.h"

struct editorstate;

struct state *editorstate_create();

#endif
