#include "play.h"

#include <stdbool.h>
#include "stb/stb_ds.h"

#include "assets.h"
#include "common/graphics/util.h"
#include "common/math/rect.h"
#include "common/util.h"
#include "entities/player.h"
#include "game_save.h"
#include "joystick_binds.h"
#include "pause_play.h"
#include "ui/checkbox.h"
#include "ui/fixed.h"
#include "ui/label.h"
#include "ui/menu.h"
#include "ui/ui.h"

static bool continue_game = false;

struct playstate {
	struct game *game;
	struct world *world;

	// used to render the correct location and position the camera
	struct player *player;

	struct ui_context *ctx;
	bool debug_menu_show;
};

struct playstate *play_state = NULL;

static void
quit()
{
	if (play_state->game)
		game_stop(play_state->game);
}

void
playstate_init(struct game *game)
{
	play_state = ecalloc(1, sizeof(*play_state));

	// NOTE(lqdev):
	// the default starting location should be (6, 8), but sometimes i change
	// this to test out blocks without having to go through the entire game.
	const vec2i_t starting_loc = {6, 8};
	const vec2_t starting_offset = {32, 32};

	play_state->game = game;
	play_state->world = world_create(game);

	vec2_t pos;
	if (!continue_game || game->gamesave == NULL) {
		pos = vec2(starting_loc.x * LOCATION_WIDTH * OBSTACLE_SCALE + starting_offset.x,
		           starting_loc.y * LOCATION_HEIGHT * OBSTACLE_SCALE + starting_offset.y);
	} else {
		vec2i_t checkpoint = game->gamesave->checkpoint_position;
		pos = vec2(checkpoint.x, checkpoint.y);

		play_state->world->keys_collected = game->gamesave->keys_collected;

		// NOTE(legalprisoner8140):
		// For safety reasons.
		continue_game = false;
	}

	play_state->player = player_create(play_state->world, pos);

	world_spawn_entity(play_state->world, (struct entity *)play_state->player);

	world_go_to(play_state->world, starting_loc);

	play_state->ctx =
	    ui_create(game->screen, g_assets.ui_font, &game->input, &g_assets.main_style);
	play_state->debug_menu_show = false;

	// I left it here for outside testing
	// and for future reference.
	// if (game->gamesave != NULL) {
	//	play_state->player->body->position = (vec2_t){
	//	    game->gamesave->checkpoint_position.x,
	//	    game->gamesave->checkpoint_position.y};
	//}

	input_on_quit(&game->input, quit);
	joystick_set_binds_game(&game->input);
}

void
playstate_clean()
{
	if (!play_state)
		return;

	world_clean(&play_state->world);
	free(play_state->ctx);

	free(play_state);
	play_state = NULL;
}

void
playstate_pause()
{
}

void
playstate_resume(struct pauseplaystate_data *data)
{
	joystick_set_binds_game(&play_state->game->input);
	input_on_quit(&play_state->game->input, quit);

	// Ignore NULL
	if (!data)
		return;

	// If player selected "Return to the checkpoint", restore player position from gamesave
	if (data->back_to_checkpoint) {
		vec2i_t pos = play_state->game->gamesave->checkpoint_position;
		player_set_pos(play_state->player, pos);
	}
}

void
playstate_update(struct game *game)
{
	assert(game != NULL);

	struct player *player = play_state->player;

	// Switch to pause menu
	if (input_key(&game->input, KEY_ENTER, STATE_JUST_PRESSED) ||
	    input_key(&game->input, KEY_ESC, STATE_JUST_PRESSED)) {
		game_push_state(game, pauseplaystate_create());
	}

	if (input_key(&game->input, KEY_CHEAT_DEBUG_MENU, STATE_JUST_PRESSED)) {
		play_state->debug_menu_show = !play_state->debug_menu_show;
	}

	if (input_key(&game->input, KEY_CHEAT_FLYHACK, STATE_JUST_PRESSED)) {
		player->cheats ^= CHEAT_FLYHACK;
	}
	if (input_key(&game->input, KEY_CHEAT_GODMODE, STATE_JUST_PRESSED)) {
		player->cheats ^= CHEAT_GODMODE;
	}
	if (input_key(&game->input, KEY_CHEAT_PHYSICS_VERBOSE_ENTITIES, STATE_JUST_PRESSED)) {
		g_config.physics_verbose ^= PHYSICS_VERBOSE_ENTITIES;
	}
	if (input_key(&game->input, KEY_CHEAT_PHYSICS_VERBOSE_TILES, STATE_JUST_PRESSED)) {
		g_config.physics_verbose ^= PHYSICS_VERBOSE_TILES;
	}
	if (input_key(&game->input, KEY_CHEAT_DISABLE_SHADOWS, STATE_JUST_PRESSED)) {
		g_config.disable_shadows = !g_config.disable_shadows;
	}

	if (input_key(&game->input, KEY_CHEAT_LOCATION_LEFT, STATE_JUST_PRESSED)) {
		player->body->position.x -= 320;
	}
	if (input_key(&game->input, KEY_CHEAT_LOCATION_RIGHT, STATE_JUST_PRESSED)) {
		player->body->position.x += 320;
	}
	if (input_key(&game->input, KEY_CHEAT_LOCATION_UP, STATE_JUST_PRESSED)) {
		player->body->position.y -= 180;
	}
	if (input_key(&game->input, KEY_CHEAT_LOCATION_DOWN, STATE_JUST_PRESSED)) {
		player->body->position.y += 180;
	}

	if (input_key(&game->input, KEY_STATS, STATE_JUST_PRESSED)) {
		g_config.stats = !g_config.stats;
	}

	// set the correct location based on the player's position
	vec2i_t loc_pos;
	player_get_location_pos(play_state->player, &loc_pos);
	world_go_to(play_state->world, loc_pos);

	world_update(play_state->world);

	struct ui_context *ctx = play_state->ctx;

	// UI
	ui_begin(ctx);

	if (g_config.stats) {
		ui_fixed_begin(ctx, vec2i(2, 2), 3);
		{
			// NOTE(legalprisoner8140):
			// Oh, so you want to add something useful under F3?
			// This is where the f u n begins!
			char buf[256];
			struct color_rgb color = color_rgb(255, 0, 0);

			vec2i_t loc_pos = play_state->world->current_loc_pos;
			snprintf(buf, 256, "location: x %d, y %d loaded: %u", loc_pos.x, loc_pos.y,
			         (uint32_t)arrlenu(play_state->world->loaded_locations));
			ui_label_color(ctx, buf, color);

			int len = arrlen(play_state->world->entities);
			snprintf(buf, 256, "entities: %d", len);
			ui_label_color(ctx, buf, color);
		}
		ui_fixed_end(ctx);
	}

	if (play_state->debug_menu_show) {
		ui_menu_begin(ctx, vec2i(5, 5), 3);
		{
			// NOTE(legalprisoner8140):
			// Alternative name: fly hack
			if (ui_checkbox(ctx, "1", CHEAT_ENABLED(player->cheats, CHEAT_FLYHACK))) {
				player->cheats ^= CHEAT_FLYHACK;
			}
			if (ui_checkbox(ctx, "godmode", CHEAT_ENABLED(player->cheats, CHEAT_GODMODE))) {
				player->cheats ^= CHEAT_GODMODE;
			}
			if (ui_checkbox(ctx, "entities physics",
			                g_config.physics_verbose & PHYSICS_VERBOSE_ENTITIES)) {
				g_config.physics_verbose ^= PHYSICS_VERBOSE_ENTITIES;
			}
			if (ui_checkbox(ctx, "tiles physics",
			                g_config.physics_verbose & PHYSICS_VERBOSE_TILES)) {
				g_config.physics_verbose ^= PHYSICS_VERBOSE_TILES;
			}
			if (ui_checkbox(ctx, "disable shadows", g_config.disable_shadows)) {
				g_config.disable_shadows = !g_config.disable_shadows;
			}
		}
		ui_menu_end(ctx);
	}
}

void
playstate_draw(struct game *game, float step)
{
	assert(game != NULL);

	// render the world
	world_render(play_state->world, game->screen, step);

	if (g_config.physics_verbose) {
		physics_space_render(play_state->world->space, game->screen);
	}

	ui_render(play_state->ctx);
}

struct state *
playstate_create(int flags)
{
	continue_game = (flags & PLAYSTATE_CONTINUE);

	return state_create((init_fn)playstate_init, (clean_fn)playstate_clean,
	                    (pause_fn)playstate_pause, (resume_fn)playstate_resume,
	                    (update_fn)playstate_update, (draw_fn)playstate_draw);
}
