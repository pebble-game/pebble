#include "menu.h"

#include <math.h>

#include "backends/audio/audio.h"
#include "build_config.h"
#include "common/graphics/surface.h"
#include "common/graphics/text.h"
#include "common/timing.h"
#include "game_save.h"
#include "joystick_binds.h"
#include "sound_system.h"

enum menu_element {
	MENU_OPEN_PMENU,
	MENU_EXIT,

	MENU_ELEMENT__LAST,
};

enum plated_menu_element {
	PMENU_CONTINUE,
	PMENU_PLAY,
	PMENU_EDITOR,
	PMENU_SETTINGS,
	PMENU_GO_BACK,

	PMENU_ELEMENT__LAST
};

const char *menu_element_names[MENU_ELEMENT__LAST] = {
    "Start game",
    "Exit game",
};

const char *pmenu_element_names[PMENU_ELEMENT__LAST] = {"Continue", "Play", "Editor",
                                                        "Settings", "Go back"};

struct plated_menu {
	struct surface *texts[PMENU_ELEMENT__LAST];
	enum plated_menu_element current;
	float current_smooth;
	float arrow_animation;
};

struct menustate {
	struct surface *surf_texts[MENU_ELEMENT__LAST];
	struct surface *surf_pebble_version;
	vec2_t pos[MENU_ELEMENT__LAST];
	enum menu_element current;

	int current_alpha;

	float y_offset;
	float y_offset_prev;
	float y_offset_speed;

	float fm_x_offset_target;
	float fm_x_offset;
	float fm_arrow_current;
	float fm_arrow_speed;

	int current_tick;
	int current_bob;

	struct plated_menu *pmenu;

	struct game *game;
	struct audio_music *music_menu;
};

struct menustate *menu_state = NULL;

static void
quit(void)
{
	if (menu_state->game)
		game_stop(menu_state->game);
}

static void
menu_music_stop(void)
{
	audio_music_clean(&menu_state->music_menu);
}

static void
menu_music_start(void)
{
	if (menu_state->music_menu)
		menu_music_stop();

	menu_state->music_menu =
	    assets_load_music(menu_state->game->audio_ctx, "audio/main_menu.opus");

	audio_music_play(menu_state->music_menu, true);
}

static struct plated_menu *
plated_menu_create(void)
{
	struct plated_menu *pmenu = ecalloc(1, sizeof(struct plated_menu));
	// Generate text
	struct text_render_settings settings = text_render_settings_defaults();
	settings.outlined = true;

	settings.color = (struct color_rgb){19, 255, 8};
	pmenu->texts[0] = text_render(g_assets.main_font, pmenu_element_names[0], &settings);
	pmenu->texts[1] = text_render(g_assets.main_font, pmenu_element_names[1], &settings);

	settings.color = (struct color_rgb){220, 171, 107};
	pmenu->texts[2] = text_render(g_assets.main_font, pmenu_element_names[2], &settings);

	settings.color = (struct color_rgb){22, 253, 249};
	pmenu->texts[3] = text_render(g_assets.main_font, pmenu_element_names[3], &settings);

	settings.color = (struct color_rgb){231, 0, 2};
	pmenu->texts[4] = text_render(g_assets.main_font, pmenu_element_names[4], &settings);

	return pmenu;
}

static void
plated_menu_clean(struct plated_menu **_pmenu)
{
	if (!_pmenu)
		return;

	struct plated_menu *pmenu = *_pmenu;
	if (!pmenu) {
		*_pmenu = NULL;
		return;
	}

	for (uint32_t i = 0; i < PMENU_ELEMENT__LAST; i++) {
		surface_clean(&pmenu->texts[i]);
	}

	free(pmenu);
	*_pmenu = NULL;
}

void
menustate_init(struct game *game)
{
	if (menu_state)
		abort();
	menu_state = ecalloc(1, sizeof(struct menustate));

	struct text_render_settings settings = text_render_settings_defaults();
	settings.outlined = true;

	settings.color = (struct color_rgb){21, 255, 252};
	menu_state->surf_texts[0] =
	    text_render(g_assets.main_font, menu_element_names[0], &settings);

	settings.color = (struct color_rgb){232, 0, 4};
	menu_state->surf_texts[1] =
	    text_render(g_assets.main_font, menu_element_names[1], &settings);

	float vertical_center = 180 / 2.0f;
	const float spacing = 14;
	float y = (vertical_center - spacing * MENU_ELEMENT__LAST / 2) + 68;
	for (int i = 0; i < MENU_ELEMENT__LAST; ++i) {
		menu_state->pos[i] = vec2(0, y);
		y += spacing;
	}

	settings.color = (struct color_rgb){255, 132, 46};
	settings.height = 10;

	menu_state->surf_pebble_version =
	    text_render(g_assets.main_font, "pebble v" VERSION, &settings);

	menu_state->current_alpha = 255;

	menu_state->fm_x_offset_target = (int)(320 / 2) - (int)(140 / 2);
	menu_state->fm_x_offset = 320 - 40;

	menu_state->fm_arrow_current = menu_state->pos[0].y;
	menu_state->fm_arrow_speed = 1;

	menu_state->current = MENU_OPEN_PMENU;
	menu_state->game = game;

	menu_music_start();

	// Move to other state
	char *path = game_save_get_path(GAME_SAVE_FILE_NAME);
	menu_state->game->gamesave = game_save_load_file(path);
	free(path);

	// Register event callbacks
	input_on_quit(&game->input, quit);

	joystick_set_binds_menu(&game->input);
}

void
menustate_clean(void)
{
	menu_music_stop();

	for (int i = 0; i < MENU_ELEMENT__LAST; ++i) {
		surface_clean(&menu_state->surf_texts[i]);
	}

	surface_clean(&menu_state->surf_pebble_version);
	plated_menu_clean(&menu_state->pmenu);

	menu_music_stop();

	free(menu_state);
	menu_state = NULL;
}

void
menustate_pause(void)
{
	menu_music_stop();
	plated_menu_clean(&menu_state->pmenu);
}

void
menustate_resume(void)
{
	menu_music_start();
	input_on_quit(&menu_state->game->input, quit);
	joystick_set_binds_menu(&menu_state->game->input);
}

void
menustate_handle_events(struct game *game)
{
	assert(game != NULL);
}

void
plated_menu_update(struct plated_menu *pmenu, struct game *game)
{
	pmenu->arrow_animation *= 0.91f;

	if (input_key(&game->input, KEY_UP, STATE_JUST_PRESSED)) {
		if (pmenu->current > 0) {
			sound_system_play(game->sound_sys, SAMPLE_MENU_SELECTION);
			pmenu->current--;
			pmenu->arrow_animation = 1.0f;
		}
	}
	if (input_key(&game->input, KEY_DOWN, STATE_JUST_PRESSED)) {
		if (pmenu->current < PMENU_ELEMENT__LAST - 1) {
			sound_system_play(game->sound_sys, SAMPLE_MENU_SELECTION);
			pmenu->current++;
			pmenu->arrow_animation = 1.0f;
		}
	}

	pmenu->current_smooth += ((float)pmenu->current - pmenu->current_smooth) * 0.2f;

	if (input_key(&game->input, KEY_ENTER, STATE_JUST_PRESSED)) {
		switch (pmenu->current) {
		case PMENU_CONTINUE:
			if (game->gamesave)
				game_push_state(game, playstate_create(PLAYSTATE_CONTINUE));
			break;
		case PMENU_PLAY:
			game_push_state(game, playstate_create(PLAYSTATE_NONE));
			break;
		case PMENU_EDITOR:
			game_push_state(game, editorstate_create());
			break;
		case PMENU_SETTINGS:
			// TODO: Add settings panel, see at issue #13
			LOG_INFO("Not implemented yet");
			break;
		case PMENU_GO_BACK:
			plated_menu_clean(&menu_state->pmenu);
			break;
		default:
			assert(false);
		}
		sound_system_play(game->sound_sys, SAMPLE_MENU_SELECTED);
	}
}

void
menustate_update(struct game *game)
{
	assert(game != NULL);

	// For interpolation purposes
	menu_state->y_offset_prev = menu_state->y_offset;

	if (!menu_state->pmenu) {
		if (input_key(&game->input, KEY_UP, STATE_JUST_PRESSED) && menu_state->current > 0) {
			menu_state->current--;
			sound_system_play(game->sound_sys, SAMPLE_MENU_SELECTION);
		}
		if (input_key(&game->input, KEY_DOWN, STATE_JUST_PRESSED) &&
		    menu_state->current < MENU_ELEMENT__LAST - 1) {
			menu_state->current++;
			sound_system_play(game->sound_sys, SAMPLE_MENU_SELECTION);
		}

		if (input_key(&game->input, KEY_ENTER, STATE_JUST_PRESSED)) {
			switch (menu_state->current) {
			case MENU_OPEN_PMENU:
				menu_state->pmenu = plated_menu_create();
				sound_system_play(game->sound_sys, SAMPLE_MENU_DRUMS);
				break;
			case MENU_EXIT:
				quit();
				break;
			default:
				assert(false);
			}
			sound_system_play(game->sound_sys, SAMPLE_MENU_SELECTION);
		}
	} else {
		plated_menu_update(menu_state->pmenu, game);
	}

	// Fade effect
	if (menu_state->current_alpha != 0) {
		menu_state->current_alpha -= 1;
	}

	// Slide down
	if (menu_state->y_offset < 360) {
		if (menu_state->y_offset_speed > 0.0f) {
			menu_state->y_offset_speed = 0.0f;
			menu_state->y_offset = 360;
		}
		menu_state->y_offset -= menu_state->y_offset_speed;
		menu_state->y_offset_speed -= 0.0085f;
		if (menu_state->y_offset > 230) {
			menu_state->y_offset_speed += 0.0235f;
		}
	} else {
		menu_state->y_offset = 360;
	}

	if (menu_state->current_tick >= 207) {
		if (menu_state->fm_x_offset != menu_state->fm_x_offset_target)
			menu_state->fm_x_offset -= 1;
	}

	if ((int)menu_state->fm_arrow_current != menu_state->pos[menu_state->current].y) {
		if (menu_state->fm_arrow_speed > 0.3) {
			if (menu_state->current == 0) {
				menu_state->fm_arrow_current -= menu_state->fm_arrow_speed;
			} else {
				menu_state->fm_arrow_current += menu_state->fm_arrow_speed;
			}
			menu_state->fm_arrow_speed -= 0.03;
		} else {
			menu_state->fm_arrow_speed += fabs(menu_state->fm_arrow_speed) + 0.3;
		}

	} else {
		menu_state->fm_arrow_current = menu_state->pos[menu_state->current].y;
		menu_state->fm_arrow_speed = 1;
	}

	float calc_sin = (sinf(time_get_seconds() * 3.0) * 780.0) / 90.0;
	menu_state->pos[0].x = calc_sin;
	menu_state->pos[1].x = calc_sin;

	menu_state->current_tick += 1;

	if (menu_state->current_tick % 10 == 0) {
		if (menu_state->current_bob == 7) {
			menu_state->current_bob = 0;
		} else {
			menu_state->current_bob += 1;
		}
	}
}

void
plated_menu_draw(struct plated_menu *pmenu, struct game *game, float step, int pos_x, int pos_y)
{
	(void)step;

	// Plate
	surface_blit(g_assets.menu_plate, NULL, game->screen, vec2i(pos_x, pos_y));

	int x_offset = 34;
	int y_offset = 33;
	const int spacing = 20;

	// Arrow
	vec2i_t arrow_pos = {x_offset - 16, y_offset + pmenu->current_smooth * spacing - 4};
	struct recti srcrect;
	srcrect.x = 0;
	srcrect.y = 0;
	srcrect.w = (1.0f - pmenu->arrow_animation) * (g_assets.menu_plate_arrow->width - 10) + 10;
	srcrect.h = g_assets.menu_plate_arrow->height;
	surface_blit(g_assets.menu_plate_arrow, &srcrect, game->screen, arrow_pos);

	// Text
	for (int i = 0; i < PMENU_ELEMENT__LAST; ++i) {
		vec2_t pos = {(float)x_offset, (float)y_offset + i * spacing};
		if (i == (int)pmenu->current) {
			pos.x += 3.0f + (pmenu->arrow_animation) * 4.0f;
		}
		surface_blit(pmenu->texts[i], NULL, game->screen, to_vec2i(pos));
	}
}

void
menustate_draw(struct game *game, float step)
{
	float y_offset = lerpf(menu_state->y_offset_prev, menu_state->y_offset, step) - 0.1f;

	// Background
	surface_blit(g_assets.menu_background, NULL, game->screen, vec2i(0, 0 - y_offset));

	// Foreground
	surface_blit(g_assets.menu_foreground, NULL, game->screen, vec2i(0, 180 - y_offset));

	// BOB
	surface_blit(g_assets.player[PLAYER_STAND][menu_state->current_bob], NULL, game->screen,
	             vec2i(200, 445 - y_offset));

	// Pebble team presents
	surface_blit(g_assets.menu_pebble_team_logo, NULL, game->screen,
	             vec2i(50 - y_offset / 2, 100 - y_offset));

	// PB Logo big
	{
		// Sine effect
		struct recti rect;
		rect.y = 0;
		rect.w = 1;
		rect.h = g_assets.menu_game_logo->height;
		for (uint32_t i = 0; i < g_assets.menu_game_logo->width; i++) {
			rect.x = i;
			vec2i_t dstpos;
			dstpos.x =
			    (320 / 2.0f - g_assets.menu_game_logo->width / 2.0f) + i + y_offset - 362;
			dstpos.y = 410 - y_offset -
			           g_assets.menu_game_logo->height / 2.0f *
			               (1 + (-y_offset + 360) *
			                        sin(menu_state->current_tick * 0.1 + i * 0.2) * 0.01);
			surface_blit(g_assets.menu_game_logo, &rect, game->screen, dstpos);
		}
	}

	// Pebble version
	surface_blit(menu_state->surf_pebble_version, NULL, game->screen,
	             vec2i((y_offset - 360.0f) / 2.0f + 2, 360 - y_offset));

	// Text
	for (int i = 0; i < MENU_ELEMENT__LAST; ++i) {
		vec2_t pos = menu_state->pos[i];

		surface_blit(menu_state->surf_texts[i], NULL, game->screen,
		             vec2i((menu_state->fm_x_offset +
		                    ((140.0f / 2) - (int)(menu_state->surf_texts[i]->width / 2))) +
		                       pos.x,
		                   ((pos.y + 3) + 358) - y_offset));
	}

	int y = (358 + menu_state->fm_arrow_current) - y_offset;

	// Arrows
	surface_blit(g_assets.menu_arrows, NULL, game->screen, vec2i(menu_state->fm_x_offset, y));

	// Plated menu
	if (menu_state->pmenu)
		plated_menu_draw(menu_state->pmenu, game, step, 10, 20);

	// Fade foreground
	surface_fill_blend(game->screen, NULL, 0, 0, 0, menu_state->current_alpha);
}

struct state *
menustate_create(void)
{
	return state_create((init_fn)menustate_init, (clean_fn)menustate_clean,
	                    (pause_fn)menustate_pause, (resume_fn)menustate_resume,
	                    (update_fn)menustate_update, (draw_fn)menustate_draw);
}
