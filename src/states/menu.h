#ifndef MENU_STATE_H
#define MENU_STATE_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assets.h"
#include "backends/render/render.h"
#include "common/common.h"
#include "editor.h"
#include "game.h"
#include "misc/config.h"
#include "play.h"
#include "states.h"

struct menustate;

struct state *menustate_create();

#endif
