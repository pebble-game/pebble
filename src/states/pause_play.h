#ifndef PAUSE_GAME_STATE_H
#define PAUSE_GAME_STATE_H

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "assets.h"
#include "backends/render/render.h"
#include "common/common.h"
#include "game.h"
#include "menu.h"
#include "misc/config.h"
#include "play.h"
#include "states.h"

struct pauseplaystate_data {
	bool back_to_checkpoint;
};

struct pauseplaystate;

struct state *pauseplaystate_create();

#endif
