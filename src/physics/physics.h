#ifndef PHYSICS_H
#define PHYSICS_H

#include <stdint.h>

#include "common/math/math.h"
#include "map/location.h"

// one extra slot for the sentinel
#define PHYSICS_MAX_CALLBACKS 16 + 1

// change this to something larger if necessary
typedef uint32_t collision_mask_t;

typedef uint16_t collision_group_t;

enum collision_state { // bitset
	NO_COLLISION = 0x0,
	JUST_STARTED_COLLIDING = 0x1,
	IS_COLLIDING = 0x2,
};

enum wall {
	WALL_TOP,
	WALL_RIGHT,
	WALL_BOTTOM,
	WALL_LEFT,

	WALL__LAST
};

struct physics_body;

typedef void (*obstacle_collision_callback_fn)(struct physics_body *b, struct obstacle,
                                               vec2i_t);

typedef void (*obstacle_collision_callback_fn_v2)(struct physics_body *b, struct obstacle,
                                                  vec2i_t, enum wall);

struct obstacle_collision_callback {
	/// The implementation of the callback.
	obstacle_collision_callback_fn impl;

	/// The filter for the callback.
	collision_mask_t mask;

	/// Controls whether duplicate calls within the same tick should be
	/// prevented. Defaults to true.
	bool prevent_duplicate_calls;

	/// private
	// used to track whether the callback was already called during the current
	// tick to prevent duplicate calls
	bool called;
};

typedef void (*body_collision_callback_fn)(struct physics_body *b, struct physics_body *other);

struct body_collision_callback {
	/// The implementation of the callback.
	body_collision_callback_fn impl;

	/// The filter for the callack.
	collision_mask_t mask;
};

struct physics_body {
	vec2_t position, velocity, force;
	vec2_t size;

	/// Used for interpolation.
	vec2_t position_prev;

	/// Coefficient of elasticity, defines how bouncy the body is.
	float elasticity;

	/// Set this to `true` to mark the body for removal.
	bool remove;

	/// Used to identify bodies uniquely in callbacks.
	/// This is initialized to 0 by default.
	collision_group_t group;

	/// Used to prevent collision response with certain objects.
	/// These fields are initialized to `~((collision_mask_t)0)` by default,
	/// so that all collisions happen.
	collision_mask_t obstacle_response_mask, body_response_mask;

	/// Collision mask for filtering collision callbacks.
	/// Defaults to 0, which means that no bodies will retrieve callbacks when
	/// colliding with the body.
	collision_mask_t collision_callback_mask;

	/// A user pointer, initialized to `NULL` by default.
	void *user;

	/// private
	enum collision_state collisions[WALL__LAST];

	// these use .impl == NULL as a sentinel
	struct obstacle_collision_callback obstacle_callbacks[PHYSICS_MAX_CALLBACKS];
	struct body_collision_callback body_callbacks[PHYSICS_MAX_CALLBACKS];

	/// private
	// used by physics_space_render
	bool collides;
};

struct physics_space {
	struct physics_body **bodies; // stb arr
	struct world *world;
};

/// @brief Creates a new physics space.
///
/// @param world the world to use for tilemap collisions
/// @param current_loc_var address to the variable holding the preprocessed
/// location
/// @return Valid pointer on success, NULL on error.
struct physics_space *physics_space_create(struct world *world);

/// @brief Frees the physics space and any associated physics bodies.
///
/// @param s the physics space to clean up
void physics_space_clean(struct physics_space **s);

/// @brief Ticks the physics space.
///
/// @param s the space to tick.
void physics_space_tick(struct physics_space *s);

void physics_space_render(struct physics_space *s, struct surface *screen);

/// @brief Creates a physics body and adds it to the space.
/// The body can be later removed by setting its `remove` field to `true`.
///
/// @param s the physics space
/// @return The newly created physics body, NULL on error.
struct physics_body *physics_body_create(struct physics_space *s);

/// @brief Returns the hitbox of the body.
///
/// @param b the body
/// @return the rect with the body's hitbox
inline struct rectf
physics_body_hitbox(struct physics_body *b)
{
	return rectf(b->position.x, b->position.y, b->size.x, b->size.y);
}

/// @brief Returns the collision state of the body.
///
/// @param b the body
/// @param wall the wall to query
/// @param mask bitwise OR'd conditions from the collision_state enum
/// @return whether any of the given conditions are met
inline bool
physics_body_collision(struct physics_body *b, enum wall wall, enum collision_state mask)
{
	return (b->collisions[wall] & mask) != 0;
}

/// @brief Interpolates between the previous and current position.
///
/// This is used for smoothing out stuttery movement caused by fixed timestep.
///
/// @param b the body
/// @param step the step between previous and current frame
/// @return the interpolated position.
inline vec2_t
physics_body_interpolated_position(struct physics_body *b, float step)
{
	return vec2(lerpf(b->position_prev.x, b->position.x, step),
	            lerpf(b->position_prev.y, b->position.y, step));
}

/// @brief Adds a body-obstacle collision callback.
///
/// The callback is only called for obstacles that match the given mask.
/// In any case, the callback also receives the obstacle if extra filtering
/// needs to be done.
///
/// If `prevent_duplicate_calls` is true, the collision callback may only be
/// called once per physics tick. This value is enabled by default and may be
/// changed in the returned struct.
///
/// There is a limit to how many callbacks may be added to a body. This limit
/// is controlled by the `PHYSICS_MAX_CALLBACKS` #define. If the limit is
/// exceeded, this function aborts the game.
///
/// @param b the body
/// @param impl the callback
/// @param mask the mask for filtering collisions
/// @returns the callback slot for adjusting extra settings
struct obstacle_collision_callback *
physics_body_obstacle_callback(struct physics_body *b, obstacle_collision_callback_fn impl,
                               collision_mask_t mask);

struct obstacle_collision_callback *
physics_body_obstacle_callback_v2(struct physics_body *b, obstacle_collision_callback_fn impl,
                                  collision_mask_t mask);

/// @brief Adds a body-body collision callback.
///
/// The callback is only called for bodies that match the given mask.
///
/// The maximum amount of body-body callbacks that can be added is defined,
/// just like with body-obstacle callbacks, by the `PHYSICS_MAX_CALLBACKS`
/// #define. If the amount is exceeded, this function aborts the game.
///
/// @param b the body
/// @param impl the callback
/// @param mask the mask for filtering collisions
void physics_body_body_callback(struct physics_body *b, body_collision_callback_fn impl,
                                collision_mask_t mask);

#endif
