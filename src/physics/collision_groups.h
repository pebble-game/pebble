#ifndef COLLISION_GROUPS_H
#define COLLISION_GROUPS_H

#include "./physics.h"
#include "map/location.h"
#include "map/obstacle_data.h"

enum {
	/// Indicates no group.
	GROUP_NONE = 0,

	GROUP_PLAYER = 1,
	GROUP_TELEPORTER = 2,
	GROUP_PROJECTILE = 3,
};

enum { // bitset
	/// Indicates no collision with other physics bodies.
	BODY_COLLISION_NONE = 0x0,

	/// Indicates collision with the player.
	BODY_COLLISION_PLAYER = 0x1,

	/// Indicates collision with projectiles
	BODY_COLLISION_PROJECTILES = 0x2,
};

/// Shortcut for OR'ing all the collision groups together.
#define BODY_COLLISION_ALL 0xFFFFFFFFu

enum { // bitset
	/// Indicates no collision with obstacles on the map.
	OBSTACLE_COLLISION_NONE = 0x0,

	/// Indicates collision with all blocks that have the IS_SOLID flag.
	OBSTACLE_COLLISION_SOLID = 0x1,

	/// Indicates collision with all blocks that kill the player.
	OBSTACLE_COLLISION_DEADLY = 0x2,

	/// Indicates collision with "kinetics" (blocks with red dots that add to
	/// your
	/// velocity).
	OBSTACLE_COLLISION_KINETIC = 0x4,

	/// Indicates collision with "movers" (blocks with arrows that change your
	/// position directly).
	OBSTACLE_COLLISION_MOVER = 0x8,

	/// Indicates collision with water block
	OBSTACLE_COLLISION_WATER = 0x10
};

/// Shortcut for OR'ing all the collision groups together.
#define OBSTACLE_COLLISION_ALL 0xFFFFFFFFu

static inline collision_mask_t
get_collision_mask_for_obstacle(struct obstacle *o)
{
	switch (o->id) {

	case ID_SPIKE_UP:
	case ID_SPIKE_RIGHT:
	case ID_SPIKE_DOWN:
	case ID_SPIKE_LEFT:
	case ID_LAVA:
		return OBSTACLE_COLLISION_DEADLY;

	case ID_MOVER_UP:
	case ID_MOVER_RIGHT:
	case ID_MOVER_DOWN:
	case ID_MOVER_LEFT:
		return OBSTACLE_COLLISION_MOVER;

	case ID_KINETIC_RIGHT:
	case ID_KINETIC_DOWN:
	case ID_KINETIC_LEFT:
	case ID_KINETIC_UP:
		return OBSTACLE_COLLISION_KINETIC;

	case ID_WATER:
		return OBSTACLE_COLLISION_WATER;

	default:
		return (o->flags & FLAGS_IS_SOLID) ? OBSTACLE_COLLISION_SOLID : OBSTACLE_COLLISION_NONE;
	}
}

#endif
