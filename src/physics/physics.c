#include "physics.h"

#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include "stb/stb_ds.h"

#include "collision_groups.h"
#include "common/graphics/util.h"
#include "common/graphics/transform.h"
#include "common/util.h"
#include "common/log.h"
#include "game.h"
#include "map/location.h"
#include "map/world.h"

extern inline struct rectf physics_body_hitbox(struct physics_body *b);

extern inline bool physics_body_collision(struct physics_body *b, enum wall wall,
                                          enum collision_state mask);

extern inline vec2_t physics_body_interpolated_position(struct physics_body *b,
                                                             float step);

struct physics_space *
physics_space_create(struct world *world)
{
	// calloc clears the memory so we don't need to do it ourselves
	struct physics_space *s = ecalloc(1, sizeof(*s));
	s->world = world;
	return s;
}

void
physics_space_clean(struct physics_space **_space)
{
	if (!_space)
		return;

	struct physics_space *space = *_space;
	assert(space);

	for (int i = 0; i < arrlen(space->bodies); i++) {
		free(space->bodies[i]);
	}
	arrfree(space->bodies);
	space->bodies = NULL;

	free(space);

	*_space = NULL;
}

static inline struct recti
align_to_tiles(struct rectf *r)
{
	const float grid_size = (float)OBSTACLE_SCALE;
	return recti_sides((int)floorf(rleftf(*r) / grid_size), (int)floorf(rtopf(*r) / grid_size),
	                   (int)ceilf(rrightf(*r) / grid_size),
	                   (int)ceilf(rbottomf(*r) / grid_size));
}

static inline struct rectf
hitbox_for_tile(vec2i_t pos)
{
	return rectf((float)pos.x * OBSTACLE_SCALE, (float)pos.y * OBSTACLE_SCALE, OBSTACLE_SCALE,
	             OBSTACLE_SCALE);
}

static inline struct obstacle *
get_obstacle(struct physics_space *s, vec2i_t global_obstacle_pos)
{
	vec2i_t location_pos, obstacle_pos;
	location_pos.x = global_obstacle_pos.x / LOCATION_WIDTH;
	location_pos.y = global_obstacle_pos.y / LOCATION_HEIGHT;
	obstacle_pos.x = global_obstacle_pos.x % LOCATION_WIDTH;
	obstacle_pos.y = global_obstacle_pos.y % LOCATION_HEIGHT;

	struct location *loc = world_get_location(s->world, location_pos);
	if (!loc)
		return NULL;

	return location_get_obstacle(loc, obstacle_pos);
}

static inline bool
tile_has_collision(struct physics_space *s, vec2i_t pos)
{
	struct obstacle *o = get_obstacle(s, pos);
	if (o == NULL)
		return false;

	return o->flags & FLAGS_IS_SOLID;
}

static inline void
body_is_colliding_with(struct physics_body *b, enum wall w)
{
	b->collisions[w] &= ~JUST_STARTED_COLLIDING;
	if ((b->collisions[w] & IS_COLLIDING) == 0)
		b->collisions[w] |= JUST_STARTED_COLLIDING;
	b->collisions[w] |= IS_COLLIDING;
}

static inline void
body_is_not_colliding_with(struct physics_body *b, enum wall w)
{
	b->collisions[w] &= ~IS_COLLIDING;
}

static inline void
update_collision_arrays(struct physics_body *b, enum wall w, bool v)
{
	if (v)
		body_is_colliding_with(b, w);
	else
		body_is_not_colliding_with(b, w);
}

static inline bool
filter_collision(collision_mask_t a, collision_mask_t b)
{
	return (a & b) != 0;
}

// NOTE(liquid):
// what a god damn beast this is.
// copied directly from my own engine, but it's fairly robust so why bother
// with something else

static const float EPSILON = 0.001;

static inline void
resolve_collision_x(struct physics_body *b, struct rectf other_hitbox, bool *with_left,
                    bool *with_right)
{
	struct rectf body_hitbox = physics_body_hitbox(b);

	// body moving left -> check right wall
	if (b->velocity.x < -EPSILON) {
		struct rectf wall =
		    rectf_sides(rrightf(other_hitbox) + b->velocity.x, rtopf(other_hitbox) + 1,
		                rrightf(other_hitbox), rbottomf(other_hitbox) - 1);
		if (rintersect(&body_hitbox, &wall)) {
			b->position.x = rrightf(other_hitbox);
			b->velocity.x *= b->elasticity;
			*with_right = true;
		}
	}

	// body moving right -> check left wall
	if (b->velocity.x > EPSILON) {
		struct rectf wall =
		    rectf_sides(rleftf(other_hitbox), rtopf(other_hitbox) + 1,
		                rleftf(other_hitbox) + b->velocity.x, rbottomf(other_hitbox) - 1);
		if (rintersect(&body_hitbox, &wall)) {
			b->position.x = rleftf(other_hitbox) - b->size.x;
			b->velocity.x *= b->elasticity;
			*with_left = true;
		}
	}
}

static inline void
resolve_collision_y(struct physics_body *b, struct rectf other_hitbox, bool *with_top,
                    bool *with_bottom)
{
	struct rectf body_hitbox = physics_body_hitbox(b);

	// body moving down -> check top wall
	if (b->velocity.y > EPSILON) {
		struct rectf wall =
		    rectf_sides(rleftf(other_hitbox) + 1, rtopf(other_hitbox),
		                rrightf(other_hitbox) - 1, rtopf(other_hitbox) + b->velocity.y);
		if (rintersect(&body_hitbox, &wall)) {
			b->position.y = rtopf(other_hitbox) - b->size.y;
			b->velocity.y *= b->elasticity;
			*with_top = true;
		}
	}

	// body moving up -> check bottom wall
	if (b->velocity.y < -EPSILON) {
		struct rectf wall =
		    rectf_sides(rleftf(other_hitbox) + 1, rbottomf(other_hitbox) + b->velocity.y,
		                rrightf(other_hitbox) - 1, rbottomf(other_hitbox));
		if (rintersect(&body_hitbox, &wall)) {
			b->position.y = rbottomf(other_hitbox);
			b->velocity.y *= b->elasticity;
			*with_bottom = true;
		}
	}
}

static inline void
do_callbacks_for_obstacle(struct physics_body *b, struct obstacle o, vec2i_t pos,
                          collision_mask_t omask)
{
	for (int i = 0; i < PHYSICS_MAX_CALLBACKS; ++i) {
		struct obstacle_collision_callback *cb = &b->obstacle_callbacks[i];
		if (cb->impl == NULL)
			break;
		if (cb->prevent_duplicate_calls && cb->called)
			continue;
		if (filter_collision(omask, cb->mask)) {
			cb->impl(b, o, pos);
			cb->called = true;
		}
	}
}

static inline void
reset_obstacle_callbacks(struct physics_body *b)
{
	for (int i = 0; i < PHYSICS_MAX_CALLBACKS; ++i) {
		if (b->obstacle_callbacks[i].impl == NULL)
			break;
		b->obstacle_callbacks[i].called = false;
	}
}

typedef void (*collision_resolver_fn)(struct physics_body *, struct rectf, bool *, bool *);

static inline void
perform_collision_check_with_obstacles(struct physics_space *s, struct physics_body *b,
                                       enum wall wall_a, enum wall wall_b,
                                       collision_resolver_fn resolve)
{
	struct rectf body_hitbox = physics_body_hitbox(b);
	struct recti tile_aligned = align_to_tiles(&body_hitbox);
	bool with_a = false, with_b = false;

	for (int y = rtopi(tile_aligned); y <= rbottomi(tile_aligned); ++y) {
		for (int x = rlefti(tile_aligned); x <= rrighti(tile_aligned); ++x) {
			struct obstacle *o = get_obstacle(s, vec2i(x, y));
			if (o == NULL)
				continue;

			struct rectf tile_hitbox = hitbox_for_tile(vec2i(x, y));
			collision_mask_t omask = get_collision_mask_for_obstacle(o);

			if (!tile_has_collision(s, vec2i(x, y)) ||
			    !filter_collision(b->obstacle_response_mask, omask))
				continue;

			// NOTE(lqdev):
			// i would love to know if this is inlined but can't be bothered to
			// check. shouldn't be too much of a performance hit though. i
			// think.
			resolve(b, tile_hitbox, &with_a, &with_b);
		}
	}

	update_collision_arrays(b, wall_a, with_a);
	update_collision_arrays(b, wall_b, with_b);
}

static inline void
check_collision_x_with_obstacles(struct physics_space *s, struct physics_body *b)
{
	perform_collision_check_with_obstacles(s, b, WALL_LEFT, WALL_RIGHT, resolve_collision_x);
}

static inline void
check_collision_y_with_obstacles(struct physics_space *s, struct physics_body *b)
{
	perform_collision_check_with_obstacles(s, b, WALL_TOP, WALL_BOTTOM, resolve_collision_y);
}

static inline void
do_body_callbacks(struct physics_body *b, struct physics_body *other)
{
	for (int i = 0; i < PHYSICS_MAX_CALLBACKS; ++i) {
		struct body_collision_callback *cb = &b->body_callbacks[i];
		if (cb->impl == NULL)
			break;
		if (filter_collision(other->collision_callback_mask, cb->mask))
			cb->impl(b, other);
	}
}

static inline void
check_collision_with_bodies(struct physics_space *s, struct physics_body *b)
{
	int body_count = arrlen(s->bodies);
	for (int i = 0; i < body_count; ++i) {
		struct physics_body *other = s->bodies[i];
		if (other == b)
			continue;

		struct rectf hb = physics_body_hitbox(b), ho = physics_body_hitbox(other);
		if (rintersect(&hb, &ho)) {
			do_body_callbacks(b, other);
			b->collides = true;
			other->collides = true;
		} else {
			b->collides = false;
			other->collides = false;
		}
	}
}

static void
do_obstacle_callbacks(struct physics_space *s, struct physics_body *b)
{
	struct rectf body_hitbox = physics_body_hitbox(b);
	struct recti tile_aligned = align_to_tiles(&body_hitbox);
	// bool with_a = false, with_b = false;

	for (int y = rtopi(tile_aligned); y <= rbottomi(tile_aligned); ++y) {
		for (int x = rlefti(tile_aligned); x <= rrighti(tile_aligned); ++x) {
			struct obstacle *o = get_obstacle(s, vec2i(x, y));
			if (o == NULL)
				continue;

			struct rectf tile_hitbox = hitbox_for_tile(vec2i(x, y));
			collision_mask_t omask = get_collision_mask_for_obstacle(o);
			if (rintersect(&body_hitbox, &tile_hitbox))
				do_callbacks_for_obstacle(b, *o, vec2i(x, y), omask);
		}
	}
}

void
physics_space_tick(struct physics_space *s)
{
	int body_count = arrlen(s->bodies);
	for (int i = 0; i < body_count; ++i) {
		struct physics_body *b = s->bodies[i];

		// remove flagged bodies
		if (b->remove) {
			free(s->bodies[i]);
			s->bodies[i] = s->bodies[arrlen(s->bodies) - 1];
			arrsetlen(s->bodies, arrlen(s->bodies) - 1);
			--i;
			--body_count;
			continue;
		}

		// update velocity/force
		b->velocity.x += b->force.x;
		b->velocity.y += b->force.y;
		b->force = vec2(0, 0);

		memset(b->collisions, 0, sizeof(b->collisions));

		b->position_prev = b->position;

		// check collision with obstacles
		reset_obstacle_callbacks(b);
		b->position.x += b->velocity.x;
		check_collision_x_with_obstacles(s, b);
		b->position.y += b->velocity.y;
		check_collision_y_with_obstacles(s, b);

		// do obstacle callbacks *after* collision has been resolved
		do_obstacle_callbacks(s, b);
	}

	// perform collision checks with bodies after all the garbage ones are
	// removed
	for (int i = 0; i < body_count; ++i) {
		check_collision_with_bodies(s, s->bodies[i]);
	}
}

void 
physics_space_render(struct physics_space *s, struct surface *screen)
{
	int body_count = arrlen(s->bodies);
	for (int i = 0; i < body_count; ++i) {
		struct physics_body *b = s->bodies[i];
	
		struct recti rect;
		rect.x = b->position.x;
		rect.y = b->position.y;
		rect.w = b->size.x;
		rect.h = b->size.y;
		
		transform_stack_push(&s->world->game->tstack, (struct transform){
			.translate = s->world->camera_pos,
		});

		// Draw obstacles' hitbox
		// NOTE(legalprisoner8140):
		// I stole it from `do_obstacle_callbacks` and it works like a charm!
		if (g_config.physics_verbose & PHYSICS_VERBOSE_TILES) {
			struct rectf body_hitbox = physics_body_hitbox(b);
			struct recti tile_aligned = align_to_tiles(&body_hitbox);

			for (int y = rtopi(tile_aligned); y <= rbottomi(tile_aligned); ++y) {
				for (int x = rlefti(tile_aligned); x <= rrighti(tile_aligned); ++x) {
					struct obstacle *o = get_obstacle(s, vec2i(x, y));
					if (o == NULL)
						continue;

					struct recti tile_hitbox;

					// Cast rectf to recti
					struct rectf hitbox = hitbox_for_tile(vec2i(x, y)); 
					tile_hitbox.x = hitbox.x;
					tile_hitbox.y = hitbox.y;
					tile_hitbox.w = hitbox.w;
					tile_hitbox.h = hitbox.h;

					if (rintersect(&body_hitbox, &hitbox))
						transformed_draw_stroke_rect(&s->world->game->tstack, color_rgba(255, 0, 0, 255), screen, tile_hitbox);
					else
						transformed_draw_stroke_rect(&s->world->game->tstack, color_rgba(0, 255, 255, 255), screen, tile_hitbox);
				}
			}
		}

		// Draw entities' hitbox
		if (g_config.physics_verbose & PHYSICS_VERBOSE_ENTITIES) {
			struct color_rgba color;
			if (b->group == GROUP_PLAYER) {
				color = color_rgba(255, 255, 0, 255);
			} else {
				color = color_rgba(255, 0, 255, 255);
			}

			if (b->collides)
				transformed_draw_stroke_rect(&s->world->game->tstack, color_rgba(255, 0, 0, 255), 
				                             screen, rect);
			else {
				transformed_draw_stroke_rect(&s->world->game->tstack, color, screen, rect);
			}
		}

		transform_stack_pop(&s->world->game->tstack);
	}
}

struct physics_body *
physics_body_create(struct physics_space *s)
{
	struct physics_body *b = ecalloc(1, sizeof(*b));

	b->obstacle_response_mask = ~((collision_mask_t)0);
	b->body_response_mask = ~((collision_mask_t)0);

	arrput(s->bodies, b);

	return b;
}

struct obstacle_collision_callback *
physics_body_obstacle_callback(struct physics_body *b, obstacle_collision_callback_fn impl,
                               collision_mask_t mask)
{
	int i = 0;
	while (i < PHYSICS_MAX_CALLBACKS && b->obstacle_callbacks[i].impl != NULL)
		++i;
	assert(i < PHYSICS_MAX_CALLBACKS);

	b->obstacle_callbacks[i] = (struct obstacle_collision_callback){
	    .impl = impl,
	    .mask = mask,
	    .prevent_duplicate_calls = true,
	    .called = false,
	};

	return &b->obstacle_callbacks[i];
}

struct obstacle_collision_callback *
physics_body_obstacle_callback_v2(struct physics_body *b, obstacle_collision_callback_fn impl,
                                  collision_mask_t mask)
{
	int i = 0;
	while (i < PHYSICS_MAX_CALLBACKS && b->obstacle_callbacks[i].impl != NULL)
		++i;
	assert(i < PHYSICS_MAX_CALLBACKS);

	b->obstacle_callbacks[i] = (struct obstacle_collision_callback){
	    .impl = impl,
	    .mask = mask,
	    .prevent_duplicate_calls = true,
	    .called = false,
	};

	return &b->obstacle_callbacks[i];
}

void
physics_body_body_callback(struct physics_body *b, body_collision_callback_fn impl,
                           collision_mask_t mask)
{
	int i = 0;
	while (i < PHYSICS_MAX_CALLBACKS && b->body_callbacks[i].impl != NULL)
		++i;
	assert(i < PHYSICS_MAX_CALLBACKS);

	b->body_callbacks[i] = (struct body_collision_callback){
	    .impl = impl,
	    .mask = mask,
	};
}
